program mo_e_pos_ints
 implicit none
 call routine_grad

end
subroutine routine
  implicit none
  BEGIN_DOC
! TODO : Put the documentation of the program here
  END_DOC
  print *, 'Hello world'
!  provide ao_two_e_integrals_in_map
  double precision :: get_ao_two_e_integral, integral
  double precision, allocatable :: test_mat(:,:,:,:)
  allocate(test_mat(mo_num, mo_num, mo_num, mo_num))
  integer :: i,j,k,l,m,n,p,q
  test_mat = 0.d0
  do m = 1, ao_num
   do n = 1, ao_num
    do p = 1, ao_num
     do q = 1, ao_num
      integral = get_ao_two_e_integral(q,n,p,m,ao_integrals_map)
      ! integral = <q n | p m >
      do l = 1, mo_num
       do k = 1, mo_num
        do j = 1, mo_num
         do i = 1, mo_num
!        do j = l, l
!         do i = k, k
          test_mat(i,j,k,l) += mo_coef_transp(i,q) * mo_coef_pos_transp(j,n) * mo_coef_transp(k,p) * mo_coef_pos_transp(l,m) * integral
         enddo
        enddo
       enddo
      enddo
     enddo
    enddo
   enddo
  enddo

  double precision :: accu
  accu = 0.d0
  do l = 1, mo_num
   do k = 1, mo_num
    do j = 1, mo_num
     do i = 1, mo_num
!     do j = l, l
!      do i = k, k
        accu += dabs(  mo_two_e_pos_int(i,j,k,l) - test_mat(i,j,k,l) )
        if(dabs(test_mat(i,j,k,l)).gt.1.d-10.and.dabs(  mo_two_e_pos_int(i,j,k,l) - test_mat(i,j,k,l) ).gt.1.d-10)then
         print*,l,k
         print*,test_mat(i,j,k,l),mo_two_e_pos_int(i,j,k,l),dabs(  mo_two_e_pos_int(i,j,k,l) - test_mat(i,j,k,l) )
!         stop
        endif
     enddo
    enddo
   enddo
  enddo
  print*,'accu / mo_num**4 = ',accu / mo_num**4
end

subroutine routine_grad
 implicit none
 integer :: i,j,k,l
 do l = 1, mo_num
  do k = 1, mo_num
   do j = 1, mo_num
    do i = 1, mo_num
     if(dabs(grad_scal_ints(i,j,k,l)-grad_scal_ints(k,l,i,j)).gt.1.d-10)then
      print*,'PB ! non hermitian :'
      print*,i,j,k,l
      print*,grad_scal_ints(i,j,k,l),grad_scal_ints(k,l,i,j),dabs(grad_scal_ints(i,j,k,l)-grad_scal_ints(k,l,i,j))
     endif
     if(dabs(grad_scal_ints(i,j,k,l)-grad_scal_ints(j,i,l,k)).gt.1.d-10)then
      print*,'PB ! non symmetric :'
      print*,i,j,k,l
      print*,grad_scal_ints(i,j,k,l),grad_scal_ints(j,i,l,k),dabs(grad_scal_ints(i,j,k,l)-grad_scal_ints(j,i,l,k))
     endif
    enddo
   enddo
  enddo
 enddo

end
