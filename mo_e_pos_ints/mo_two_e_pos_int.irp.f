
BEGIN_PROVIDER [ double precision, mo_two_e_pos_int, (mo_num, mo_num, mo_num, mo_num)]
 implicit none
 BEGIN_DOC
! mo_two_e_pos_int(i,j,k,l) = \int dr1 \int dr2 \phi_i^{elec}(r1) \phi_j^{pos}(r2) 1/r12 \phi_k^{elec}(r1) \phi_l^{pos}(r2)
!
! mo_two_e_pos_int(i,j,k,l) = <phi_i^{elec} phi_j^{pos} | 1/r12 | phi_k^{elec} phi_l^{pos}) in PHYSICIST NOTATION 
!
! NOTICE THAT : because the MOs for the electrons and positrons ARE NOT THE SAME then the tensor is not symmetric in r1-r2 
!
! mo_two_e_pos_int(i,j,k,l) NOT EQUAL TO mo_two_e_pos_int(j,i,l,k) for instance 
 END_DOC
 integer :: i,j,k,l,m,n,p,q
 double precision :: integral, get_ao_two_e_integral
 double precision, allocatable :: mo_tmp_1(:,:,:,:),mo_tmp_2(:,:,:,:),mo_tmp_3(:,:,:,:),two_e_tmp_0(:,:)

 allocate(mo_tmp_1(mo_num,ao_num,ao_num,ao_num),two_e_tmp_0(ao_num,ao_num))
 mo_tmp_1 = 0.d0

 do m = 1, ao_num
  do p = 1, ao_num
   do n = 1, ao_num
     call get_ao_two_e_integrals(n,p,m,ao_num,two_e_tmp_0(1,n))
!      two_e_tmp_0(q,n) = <  q n  | p m > = (qp | nm)
   enddo
   do n = 1, ao_num
    do q = 1, ao_num
!      integral = get_ao_two_e_integral(q,n,p,m,ao_integrals_map)
!      two_e_tmp_0(q,n) = integral ! <q n | p m >
     do i = 1, mo_num
      !               <e ? | ? ? >
      !               <i n | p m > =   sum_q c_qi * < q n | p m >
      mo_tmp_1(i,n,p,m) += mo_coef_transp(i,q) * (-1.d0 ) * two_e_tmp_0(q,n) !! -1 because it is attractive 
     enddo
    enddo
   enddo
  enddo
 enddo


 allocate(mo_tmp_2(mo_num,mo_num,ao_num,ao_num))
 mo_tmp_2 = 0.d0

 do m = 1, ao_num
  do p = 1, ao_num
   do n = 1, ao_num
    do j = 1, mo_num
     do i = 1, mo_num
      !               <e p | ? ? >
      !               <i j | p m > =   sum_n c_nj * < i n | p m >
      mo_tmp_2(i,j,p,m) += mo_coef_pos_transp(j,n) * mo_tmp_1(i,n,p,m) 
     enddo
    enddo
   enddo
  enddo
 enddo



 deallocate(mo_tmp_1)
 allocate(mo_tmp_1(mo_num,mo_num,mo_num,ao_num))
 mo_tmp_1 = 0.d0
 do m = 1, ao_num
  do p = 1, ao_num
   do k = 1, mo_num
    do j = 1, mo_num
     do i = 1, mo_num
      !               <e p | e ? >
      !               <i j | k m > =   sum_p c_pk * < i j | p m >
      mo_tmp_1(i,j,k,m) += mo_coef_transp(k,p) * mo_tmp_2(i,j,p,m) 
     enddo
    enddo
   enddo
  enddo
 enddo



 deallocate(mo_tmp_2)
 mo_two_e_pos_int = 0.d0
 do m = 1, ao_num
  do l = 1, mo_num
   do k = 1, mo_num
    do j = 1, mo_num
     do i = 1, mo_num
      !               <e p | e p >
      !               <i j | k l > =   sum_m c_ml * < i j | k m >
      mo_two_e_pos_int(i,j,k,l) += mo_coef_pos_transp(l,m) * mo_tmp_1(i,j,k,m) 
     enddo
    enddo
   enddo
  enddo
 enddo


END_PROVIDER 



BEGIN_PROVIDER [double precision, diag_mo_two_e_pos_int, (mo_num, mo_num)]
 implicit none
 BEGIN_DOC
! diagonal part of the (i.e. coulomb of mo_two_e_pos_int(i,j,k,l))
!
! diag_mo_two_e_pos_int(i,j) = \int dr1 \int dr2 (\phi_i^{elec}(r1))^2 1/r12 (\phi_j^{pos}(r2))^2
!
! diag_mo_two_e_pos_int(i_elec, j_pos) 
 END_DOC
 integer :: i_elec,j_pos
 do j_pos = 1, mo_num
  do i_elec = 1, mo_num
   diag_mo_two_e_pos_int(i_elec,j_pos) = mo_two_e_pos_int(i_elec,j_pos,i_elec,j_pos)
  enddo
 enddo

END_PROVIDER 



BEGIN_PROVIDER [double precision, eff_2_idx_pos_ints, (mo_num, mo_num, mo_num)]
 implicit none
 BEGIN_DOC
! diag_mo_two_e_pos_int(i,j,k) = \int dr1 \int dr2 (\phi_i^{elec}(r1))^2 1/r12 \phi_j^{pos}(r2) \phi_k^{pos}(r2)
!
! diag_mo_two_e_pos_int(i_elec, j_pos, k_pos) 
 END_DOC
 integer :: i_elec,j_pos,k_pos
 do j_pos = 1, mo_num
  do k_pos = 1, mo_num
   do i_elec = 1, mo_num
    eff_2_idx_pos_ints(i_elec,j_pos,k_pos) = mo_two_e_pos_int(i_elec,j_pos,i_elec,k_pos)
   enddo
  enddo
 enddo

END_PROVIDER 

BEGIN_PROVIDER [double precision, eff_2_idx_e_ints, (mo_num, mo_num, mo_num)]
 implicit none
 BEGIN_DOC
! "Fock" part of the (i.e. coulomb of mo_two_e_pos_int(i,j,k,l))
!
! diag_mo_two_e_pos_int(i,j,k) = \int dr1 \int dr2 (\phi_i^{pos}(r1))^2 1/r12 \phi_j^{elec}(r2) \phi_k^{elec}(r2)
!
! diag_mo_two_e_pos_int(i_elec, j_pos, k_pos) 
 END_DOC
 integer :: i_pos,j_elec,k_elec
 do j_elec= 1, mo_num
  do k_elec= 1, mo_num
   do i_pos= 1, mo_num
    eff_2_idx_e_ints(i_pos,j_elec,k_elec) = mo_two_e_pos_int(j_elec,i_pos,k_elec,i_pos)
   enddo
  enddo
 enddo

END_PROVIDER 
