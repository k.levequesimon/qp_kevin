program k_gauss_int
  implicit none
  double precision :: k_norm,average_proba
  k_norm=k_norm_continuum
  call get_average_k_ang(k_norm,average_proba)
  if (positron.eqv..false.) open(unit=10,file="auger_electron",form="formatted")
  if (positron.eqv..true.)  open(unit=10,file="auger_positron",form="formatted")
  write(10,'(A,E25.12)') "Energy [eV]",27.2113845d0*k_norm**2.d0/2.d0
  write(10,'(A,E25.12)') "k_norm [au]",k_norm
  write(10,'(A,E25.12)') "P_aver     ",average_proba
  close(10)
end
