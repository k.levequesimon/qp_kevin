 subroutine int_k_gauss_prov(k_vec,integ_real,integ_im)
 implicit none
 BEGIN_DOC
 ! compute the integral A(k) = \int dr1 dr2 exp(-i k.r1) phi_escape(r1) \int dr2 phi_hole(r1) phi_fall(r1) 1/r12
 END_DOC
 double precision, intent(in)  :: k_vec(3)
 double precision, intent(out) :: integ_real,integ_im
 integer                       :: i
 double precision              :: weight,r(3),phi_escape,I_hole_fall
 double precision              :: phi_real,phi_im
 integ_real = 0.d0
 integ_im = 0.d0
 do i = 1,n_points_final_grid                                                                                           
  r(1) = final_grid_points(1,i)
  r(2) = final_grid_points(2,i)
  r(3) = final_grid_points(3,i)
  weight = final_weight_at_r_vector(i)
  I_hole_fall = int_coul_hole_fall(i)
  if (positron.eqv..false.) phi_escape = mos_in_r_array_transp(i,i_escape_auger)
  if (positron.eqv..true.)  phi_escape = mos_pos_in_r_array_transp(i,i_escape_auger)
  call phi_k_continuum(r,k_vec,phi_real,phi_im)
  integ_real += I_hole_fall * phi_escape * phi_real * weight
  integ_im   += I_hole_fall * phi_escape * phi_im   * weight
 enddo
end

BEGIN_PROVIDER[double precision,int_coul_hole_fall,(n_points_final_grid)]
 implicit none
 integer                      :: i
 double precision             :: r(3)
 double precision,allocatable ::  integrals_ao(:,:),integrals_mo(:,:)
 allocate(integrals_ao(ao_num,ao_num),integrals_mo(mo_num,mo_num))
 !$OMP PARALLEL                                &
 !$OMP PRIVATE (i,integrals_ao,integrals_mo,r) & 
 !$OMP SHARED (n_points_final_grid,mo_num,ao_num,final_grid_points,i_hole_auger,i_fall_auger,int_coul_hole_fall)
 !$OMP DO SCHEDULE (guided)
 do i = 1,n_points_final_grid                                                                                           
  r(1) = final_grid_points(1,i)
  r(2) = final_grid_points(2,i)
  r(3) = final_grid_points(3,i)
  call give_all_erf_kl_ao(integrals_ao,1.d+6,r)
  call ao_to_mo(integrals_ao,ao_num,integrals_mo,mo_num)
  int_coul_hole_fall(i) = integrals_mo(i_hole_auger,i_fall_auger)
 enddo
 !$OMP END DO
 !$OMP END PARALLEL
 deallocate(integrals_mo,integrals_ao)
END_PROVIDER 

subroutine phi_k_continuum(r,k_vec,phi_real,phi_im)
 implicit none
 double precision, intent(in)  :: r(3),k_vec(3)
 double precision, intent(out) :: phi_real, phi_im
 complex*16 :: phi
 phi = cdexp(dcmplx(0.d0,-(k_vec(1)*r(1)+k_vec(2)*r(2)+k_vec(3)*r(3))))
 phi_real = dreal(phi)
 phi_im   = dimag(phi)
end

subroutine get_average_k_ang(k_norm,average_proba)
 implicit none
 double precision, intent(in)  :: k_norm
 double precision, intent(out) :: average_proba
 double precision              :: k_vec(3),weight,integ_real,integ_im
 integer :: i
 average_proba = 0.d0
 do i = 1, n_points_extra_integration_angular
  k_vec(1:3) = angular_quadrature_points_extra(i,1:3) * k_norm
  call int_k_gauss_prov(k_vec,integ_real,integ_im)
  weight = weights_angular_points_extra(i)
  average_proba += weight * (integ_real**2 + integ_im**2)
 enddo
 average_proba *= 2.d0 * dacos(-1.d0)
end
