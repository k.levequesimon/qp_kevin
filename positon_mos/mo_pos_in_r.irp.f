subroutine give_all_mos_pos_at_r(r,mos_array)
 implicit none
 BEGIN_DOC
! mos_array(i) = ith MO function OF THE POSITRON BASIS evaluated at "r"
 END_DOC
 double precision, intent(in) :: r(3)
 double precision, intent(out) :: mos_array(mo_num)
 double precision :: aos_array(ao_num)
 call give_all_aos_at_r(r,aos_array)
 call dgemv('N',mo_num,ao_num,1.d0,mo_coef_pos_transp,mo_num,aos_array,1,0.d0,mos_array,1)
end


subroutine give_all_mos_pos_and_grad_at_r(r,mos_pos_array,mos_pos_grad_array)
 implicit none
 BEGIN_DOC
! mos_array(i) = ith MO function OF THE POSITRON BASIS evaluated at "r"
!
! and mos_pos_grad_array(m,i) = gradient of that MO (m=1->x, m=2->y, m=3->z)
 END_DOC
 double precision, intent(in) :: r(3)
 double precision, intent(out) :: mos_pos_array(mo_num)
 double precision, intent(out) :: mos_pos_grad_array(3,mo_num)
 integer :: i,j,k
 double precision :: aos_array(ao_num),aos_grad_array(3,ao_num)
 call give_all_aos_and_grad_at_r(r,aos_array,aos_grad_array)
 mos_pos_array=0d0
 mos_pos_grad_array=0d0
 do j = 1, mo_num
  do k=1, ao_num
   mos_pos_array(j) += mo_coef_pos(k,j)*aos_array(k)
   mos_pos_grad_array(1,j) += mo_coef_pos(k,j)*aos_grad_array(1,k)
   mos_pos_grad_array(2,j) += mo_coef_pos(k,j)*aos_grad_array(2,k)
   mos_pos_grad_array(3,j) += mo_coef_pos(k,j)*aos_grad_array(3,k)
  enddo
 enddo
end


 BEGIN_PROVIDER[double precision, mos_pos_in_r_array, (mo_num,n_points_final_grid)]
 implicit none
 BEGIN_DOC
 ! mos_pos_in_r_array(i,j)        = value of the ith mo on the jth grid point
 END_DOC
 integer :: i,j
 double precision :: mos_pos_array(mo_num), r(3)
 do i = 1, n_points_final_grid
  r(1) = final_grid_points(1,i)
  r(2) = final_grid_points(2,i)
  r(3) = final_grid_points(3,i)
  call give_all_mos_pos_at_r(r,mos_pos_array)
  do j = 1, mo_num
   mos_pos_in_r_array(j,i) = mos_pos_array(j)
  enddo
 enddo
 END_PROVIDER

 BEGIN_PROVIDER[double precision, mos_pos_in_r_array_transp,(n_points_final_grid,mo_num)]
 implicit none
 BEGIN_DOC
 ! mos_pos_in_r_array_transp(i,j) = value of the jth mo on the ith grid point
 END_DOC
 integer :: i,j
 do i = 1, n_points_final_grid
  do j = 1, mo_num
   mos_pos_in_r_array_transp(i,j) = mos_pos_in_r_array(j,i) 
  enddo
 enddo
 END_PROVIDER


 BEGIN_PROVIDER[double precision, mos_pos_grad_in_r_array,(mo_num,n_points_final_grid,3)]
 implicit none
 BEGIN_DOC
 ! mos_pos_grad_in_r_array(i,j,k)          = value of the kth component of the gradient of ith mo on the jth grid point       
 !
 ! mos_pos_grad_in_r_array_transp(i,j,k)   = value of the kth component of the gradient of jth mo on the ith grid point
 !
 ! k = 1 : x, k= 2, y, k  3, z
 END_DOC
 integer :: m
 mos_pos_grad_in_r_array = 0.d0
 do m=1,3
  call dgemm('N','N',mo_num,n_points_final_grid,ao_num,1.d0,mo_coef_pos_transp,mo_num,aos_grad_in_r_array(1,1,m),ao_num,0.d0,mos_pos_grad_in_r_array(1,1,m),mo_num)
 enddo
 END_PROVIDER

