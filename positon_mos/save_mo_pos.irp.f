subroutine save_mos_pos
  implicit none
  double precision, allocatable  :: buffer(:,:)
  integer                        :: i,j

  allocate ( buffer(ao_num,mo_num) )
  buffer = 0.d0
  do j = 1, mo_num
    do i = 1, ao_num
      buffer(i,j) = mo_coef_pos(i,j)
    enddo
  enddo
  call ezfio_set_positon_mos_mo_coef_pos(buffer)
  deallocate (buffer)

end

