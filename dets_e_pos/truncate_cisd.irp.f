program truncate_cisd
 implicit none
 read_wf = .True.
 touch read_wf 
 call truncate

end

subroutine truncate
 use bitmasks
 implicit none
 integer :: degree, i, ndet
 integer(bit_kind), allocatable :: psi_det_final(:,:,:)
 double precision, allocatable :: psi_coef_final(:,:)
 integer, allocatable :: i_index(:)
 allocate(i_index(N_det))
 ndet = 0
 do i = 1, N_det
  call get_excitation_degree(ref_bitmask,psi_det(1,1,i),degree,N_int)
  if(degree.lt.2)then
   ndet += 1
   i_index(ndet) = i
  elseif(degree==2)then
   if(dabs(psi_coef(i,1)).gt.save_threshold)then
    ndet += 1
    i_index(ndet) = i
   endif
  endif
 enddo
 allocate(psi_det_final(N_int,2,n_det),psi_coef_final(N_det,N_states))
 do i = 1, ndet
  psi_det_final(1:N_int,1:2,i) = psi_det(1:N_int,1:2,i_index(i))
  psi_coef_final(i,1:N_states) = psi_coef(i_index(i),1:N_states)
 enddo
 call save_wavefunction_general(ndet,N_states,psi_det_final,ndet,psi_coef_final)
end
