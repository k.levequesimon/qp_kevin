program test_cisd_e_pos
 implicit none
 read_wf = .True.
 touch read_wf 
 call test_on_top
end

subroutine test_on_top
 implicit none
 double precision :: weight, r(3),i, dm_a, dm_b, dm, mo_pos, dm_pos, accu
 print*,'on_top_e_pos      = ',on_top_e_pos(1)
 accu = 0.d0
 do i = 1,  n_points_final_grid
  ! you get x, y and z of the ith grid point
  r(1) = final_grid_points(1,i)
  r(2) = final_grid_points(2,i)
  r(3) = final_grid_points(3,i)
  weight = final_weight_at_r_vector(i)
  call dm_dft_alpha_beta_at_r(r,dm_a,dm_b)
  dm = dm_a + dm_b
  mo_pos = mos_pos_in_r_array_transp(i,1)
  dm_pos = mo_pos**2
  accu += dm_pos * dm * weight
 enddo
 print*,'mean-field on top = ',accu

end
