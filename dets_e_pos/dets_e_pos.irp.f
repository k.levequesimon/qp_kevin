program dets_e_pos
  implicit none
  BEGIN_DOC
! TODO : Put the documentation of the program here
  END_DOC
!  read_wf = .True.
!  touch read_wf 
!  call routine
  call routine_tests
! call save_ref_determinant_e_pos
 
end


subroutine routine
 implicit none
 integer :: i,j,ispin, i_bit
 use bitmasks
 integer           :: i_pos, j_pos
 integer(bit_kind), allocatable  :: key_i(:,:), key_j(:,:)
 double precision  :: hij
 allocate(key_i(N_int,2), key_j(N_int,2))
 print*,'n_e_pos_det = ',n_e_pos_det
 do i = 1, n_e_pos_det
!  do j = 1, n_e_pos_det
  do j = i,i
   ! copying the determinants in key_i and key_j
   print*,'i = ',i
   do ispin = 1, 2
    do i_bit = 1, N_int
     key_i(i_bit,ispin) = psi_e_pos_det(i_bit,ispin,i)
     key_j(i_bit,ispin) = psi_e_pos_det(i_bit,ispin,j)
     print*,ispin,i_bit,psi_e_pos_det(i_bit,ispin,i)
    enddo
   enddo
   print*,'key_i = '
   call debug_det(key_i,N_int)
   print*,'key_j = '
   call debug_det(key_j,N_int)
   i_pos = pos_occ(i)
   j_pos = pos_occ(j)
   print*,'i_pos, j_pos',i_pos, j_pos
   call i_H_j_e_pos(key_i,key_j,i_pos, j_pos, N_int,hij)
   print*,'i,j,hij',i,j,hij
  enddo
 enddo
end


subroutine routine_tests
 implicit none
 integer :: i,j,ispin, i_bit, i_ok,a,b,jspin,j_hole
 use bitmasks
 integer           :: i_pos, j_pos, n_det_tmp,i_hole, i_particle, j_particle
 integer, allocatable :: pos_occ_tmp(:)
 integer(bit_kind), allocatable  :: key_i(:,:), key_j(:,:)
 integer(bit_kind), allocatable  :: psi_dets_pos_tmp(:,:,:)
 double precision  :: hij, accu
 allocate(key_i(N_int,2), key_j(N_int,2))


 key_i = HF_bitmask 
 i_pos = 1
 call i_H_j_e_pos(key_i,key_i,i_pos, i_pos, N_int,hij)
 print*,'Checking the HF energy '
 print*,'hij = ', hij
 print*,''
 print*,''


  !!!!! Program to test the Brillouin theorem !!!!!
 print*,''
 print*,''
  print*,'Checking the Brillouin theorem for the positronic single excitations '
 
  !!!!! Do the single excitations on the positrons : mo_num -1 single excitations 
  n_det_tmp = mo_num - 1 + 1 ! total number of determinants 
  allocate(pos_occ_tmp(n_det_tmp), psi_dets_pos_tmp(N_int,2,n_det_tmp))
 
  do i = 1, n_det_tmp
   psi_dets_pos_tmp(:,:,i) = HF_bitmask ! initialize key_i to the HF determinant for electrons 
  enddo
  pos_occ_tmp(1) = 1 ! HF for the positron 
  j = 1
  do i = 2, mo_num
   j += 1
   pos_occ_tmp(j) = i ! occupation of the positrons 
  enddo
 
  ! key_i is the first element of the psi_dets_pos_tmp 
  key_i(:,:) = psi_dets_pos_tmp(:,:,1) ! HF electron 
  i_pos = pos_occ_tmp(1) ! HF for positron 
  accu = 0.d0
  do j = 2, n_det_tmp ! loop over determinants 
   key_j(:,:) = psi_dets_pos_tmp(:,:,j) ! copy the electronic part in key_j
   j_pos = pos_occ_tmp(j)  ! j_pos == occupation of positron in determinant j
!   call debug_det(key_j,N_int)
   call i_H_j_e_pos(key_i,key_j,i_pos, j_pos, N_int,hij)
!  print*,'j, hij = ',j, hij
   accu += dabs(hij)
  enddo
  print*,'accu / n_det_tmp = ',accu/n_det_tmp

  deallocate(psi_dets_pos_tmp,pos_occ_tmp)
 print*,''
 print*,''

 print*,'Checking the Brillouin theorem for the electronic single excitations of spin alpha '

 !!!!!!!!!!!!! building the CIS wave function for electrons
 !!!!!! Counting the number of single excitations alpha + HF 
 j = 1
 ispin = 1
 do i = 1, elec_alpha_num
  do a = elec_alpha_num+1, mo_num
   do j = 1, elec_alpha_num
    do b = elec_alpha_num+1, mo_num
     key_i = HF_bitmask ! initialize key_i to the HF determinant
     i_hole = i 
     i_particle = a
     call do_single_excitation(key_i,i_hole,i_particle,ispin,i_ok)
     j_hole = j 
     j_particle = b
     call do_single_excitation(key_i,j_hole,j_particle,ispin,i_ok)
     if(i_ok .ne. 1)then
      cycle
     endif
    
     call get_excitation(HF_bitmask,key_i,exc,degree,phase,N_int)
     call decode_exc(exc,degree,h1,p1,h2,p2,s1,s2)
     print*,'h1,p1',h1,p1
     print*,'i,a',i,a
     print*,'h2,p2',h2,p2
     print*,'j,b',j,b
    integer                        :: exc(0:2,2,2)
    double precision               :: phase
    integer                        :: h2,p2,s2,degree,h1,p1,s1
    enddo
   enddo
  enddo
 enddo
 stop

 n_det_tmp = j ! number of possible single excitations for electrons alpha + HF 
 print*,'n_det_tmp = ',n_det_tmp

 allocate(pos_occ_tmp(n_det_tmp), psi_dets_pos_tmp(N_int,2,n_det_tmp))
 ! creating all determinants for single excitation alpha
 j = 1
 ispin = 1
 psi_dets_pos_tmp(:,:,1) = HF_bitmask(:,:) ! initialize with the HF determinant
 pos_occ_tmp(1) = 1
 do i = 1, elec_alpha_num
  do a = elec_alpha_num+1, mo_num
   key_i = HF_bitmask ! initialize key_i to the HF determinant
   i_hole = i 
   i_particle = a
   call do_single_excitation(key_i,i_hole,i_particle,ispin,i_ok)
   if(i_ok == 1)then
    j += 1
    psi_dets_pos_tmp(:,:,j) = key_i(:,:)
    pos_occ_tmp(j) = 1
   endif
  enddo
 enddo

 ! key_i is the first element of the psi_dets_pos_tmp 
 key_i(:,:) = psi_dets_pos_tmp(:,:,1)
 i_pos = pos_occ_tmp(1)
 accu = 0.d0
 do j = 2, n_det_tmp
  key_j(:,:) = psi_dets_pos_tmp(:,:,j)
  j_pos = pos_occ_tmp(j) 
  call i_H_j_e_pos(key_i,key_j,i_pos, j_pos, N_int,hij)
!  call debug_det(key_j,N_int)
   print*,'j, hij = ',j, hij
   accu += dabs(hij)
 enddo
  print*,'accu / n_det_tmp = ',accu/n_det_tmp
 print*,''
 print*,''

 deallocate(psi_dets_pos_tmp,pos_occ_tmp)
 

 print*,'Checking the Brillouin theorem for the electronic single excitations of spin beta '

 !!!!!!!!!!!!! building the CIS wave function for electrons
 j = 1
 ispin = 2
 do i = 1, elec_alpha_num
  do a = elec_alpha_num+1, mo_num
   key_i = HF_bitmask ! initialize key_i to the HF determinant
   i_hole = i 
   i_particle = a
   call do_single_excitation(key_i,i_hole,i_particle,ispin,i_ok)
   if(i_ok == 1)then
    j += 1
   endif
  enddo
 enddo

 n_det_tmp = j ! number of possible single excitations excitations 

 allocate(pos_occ_tmp(n_det_tmp), psi_dets_pos_tmp(N_int,2,n_det_tmp))
 j = 1
 ispin = 2
 psi_dets_pos_tmp(:,:,1) = HF_bitmask(:,:) ! initialize with the HF determinant
 pos_occ_tmp(1) = 1
 do i = 1, elec_alpha_num
  do a = elec_alpha_num+1, mo_num
   key_i = HF_bitmask ! initialize key_i to the HF determinant
   i_hole = i 
   i_particle = a
   call do_single_excitation(key_i,i_hole,i_particle,ispin,i_ok)
   if(i_ok == 1)then
    j += 1
    psi_dets_pos_tmp(:,:,j) = key_i(:,:)
    pos_occ_tmp(j) = 1
   endif
  enddo
 enddo

 ! key_i is the first element of the psi_dets_pos_tmp 
 key_i(:,:) = psi_dets_pos_tmp(:,:,1)
 i_pos = pos_occ_tmp(1)
 accu = 0.d0
 do j = 2, n_det_tmp
! do j = 5,5
  key_j(:,:) = psi_dets_pos_tmp(:,:,j)
  j_pos = pos_occ_tmp(j) 
!  call debug_det(key_j,N_int)
  call i_H_j_e_pos(key_i,key_j,i_pos, j_pos, N_int,hij)
   accu += dabs(hij)
!  print*,'j, hij = ',j, hij
 enddo
  print*,'accu / n_det_tmp = ',accu/n_det_tmp

 deallocate(psi_dets_pos_tmp,pos_occ_tmp)



end
