program test_cisd_e_pos
 implicit none
 read_wf = .True.
 touch read_wf 
 call routine
 call test_on_top
end


subroutine routine
 implicit none
 integer :: i_e,j_pos,k_e,l_pos
 double precision :: accu
 accu = 0.d0
 do j_pos = 1, mo_num
  do l_pos = j_pos,j_pos
   do i_e = 1, mo_num
    do k_e = i_e,i_e
     accu += mo_two_rdm_e_pos(k_e,i_e,l_pos,j_pos,1)
    enddo
   enddo
  enddo
 enddo
 print*,'accu = ',accu
 accu = 0.d0
 do j_pos = 1, mo_num
  do l_pos = 1, mo_num
   do i_e = 1, mo_num
    do k_e = 1, mo_num
     accu += mo_two_rdm_e_pos(k_e,i_e,l_pos,j_pos,1) * mo_two_e_pos_int(k_e,l_pos,i_e,j_pos)
    enddo
   enddo
  enddo
 enddo
 print*,'Expectation value from RDM = ',accu
 integer :: idet, jdet
 double precision :: hij, o1_ij, o2_ij, o2_ep
 double precision, allocatable :: ints_e_ij(:,:), ints_pos_ij(:,:), ints_two_e(:,:,:,:)
 allocate(ints_e_ij(mo_num,mo_num), ints_pos_ij(mo_num,mo_num), ints_two_e(mo_num,mo_num,mo_num,mo_num))
 ints_e_ij = 0.d0
 ints_pos_ij = 0.d0
 ints_two_e = 0.d0
 accu = 0.d0
 do idet=1,N_e_pos_det
   j_pos=pos_occ(idet)
   do jdet=1,N_e_pos_det
     l_pos=pos_occ(jdet)
     call i_o1_o2_j(psi_e_pos_det(1,1,idet),psi_e_pos_det(1,1,jdet),j_pos, l_pos, & 
                    ints_e_ij, ints_pos_ij, ints_two_e, mo_two_e_pos_int, N_int,o1_ij, o2_ij, o2_ep)
     accu += o2_ep * psi_e_pos_coef(idet,1) * psi_e_pos_coef(jdet,1) 
   enddo
!     call i_o1_o2_j(psi_e_pos_det(1,1,idet),psi_e_pos_det(1,1,idet),j_pos, j_pos, & 
!                    ints_e_ij, ints_pos_ij, ints_two_e, mo_two_e_pos_int, N_int,o1_ij, o2_ij, o2_ep)
!     accu += o2_ep * psi_e_pos_coef(idet,1) **2
 enddo
 print*,'accu = ',accu

end

subroutine test_on_top
 implicit none
 double precision :: weight, r(3),i, dm_a, dm_b, dm, mo_pos, dm_pos, accu
 print*,'on_top_e_pos = ',on_top_e_pos(1)
 accu = 0.d0
 do i = 1,  n_points_final_grid
  ! you get x, y and z of the ith grid point
  r(1) = final_grid_points(1,i)
  r(2) = final_grid_points(2,i)
  r(3) = final_grid_points(3,i)
  weight = final_weight_at_r_vector(i)
  call dm_dft_alpha_beta_at_r(r,dm_a,dm_b)
  dm = dm_a + dm_b
  mo_pos = mos_pos_in_r_array_transp(i,1)
  dm_pos = mo_pos**2
  accu += dm_pos * dm * weight
 enddo
 print*,'accu = ',accu

end
