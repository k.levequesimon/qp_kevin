subroutine i_H_j_e_pos(key_i,key_j,i_pos, j_pos, Nint,hij)
  use bitmasks
  implicit none
  BEGIN_DOC
  ! Returns $\langle i|H|j \rangle$ where $i$ and $j$ are determinants.
  END_DOC
  integer, intent(in)            :: Nint, i_pos, j_pos
  integer(bit_kind), intent(in)  :: key_i(Nint,2), key_j(Nint,2)
  double precision, intent(out)  :: hij

  integer                        :: exc(0:2,2,2)
  integer                        :: degree
  double precision               :: get_two_e_integral
  integer                        :: h,n,p,q
  integer                        :: i,j,k
  integer                        :: occ(Nint*bit_kind_size,2)
  double precision               :: diag_H_mat_elem, phase
  double precision               :: pos_single_exc_H_mat_elem,pos_diag_H_mat_elem
  integer                        :: n_occ_ab(2), degree_pos, degree_tot
  PROVIDE mo_two_e_integrals_in_map mo_integrals_map big_array_exchange_integrals

  ASSERT (Nint > 0)
  ASSERT (Nint == N_int)
  ASSERT (sum(popcnt(key_i(:,1))) == elec_alpha_num)
  ASSERT (sum(popcnt(key_i(:,2))) == elec_beta_num)
  ASSERT (sum(popcnt(key_j(:,1))) == elec_alpha_num)
  ASSERT (sum(popcnt(key_j(:,2))) == elec_beta_num)


  hij = 0.d0
  !DIR$ FORCEINLINE
  call get_excitation_degree(key_i,key_j,degree,Nint)
  integer :: spin
  ! excitation degree of the positron
  degree_pos = 0
  if(i_pos .ne. j_pos)then 
   degree_pos += 1
  endif

  ! total excitation degree
  degree_tot = degree_pos + degree
  if(degree_tot.gt.2)then
   return
  endif

  
  if(degree_pos == 0)then ! purely electronic double excitation 
   select case (degree)
     case (2) 
       call get_double_excitation(key_i,key_j,exc,phase,Nint)
       if (exc(0,1,1) == 1) then
         ! Single alpha, single beta
         if(exc(1,1,1) == exc(1,2,2) )then
           hij = phase * big_array_exchange_integrals(exc(1,1,1),exc(1,1,2),exc(1,2,1))
         else if (exc(1,2,1) ==exc(1,1,2))then
           hij = phase * big_array_exchange_integrals(exc(1,2,1),exc(1,1,1),exc(1,2,2))
         else
           hij = phase*get_two_e_integral(                          &
               exc(1,1,1),                                              &
               exc(1,1,2),                                              &
               exc(1,2,1),                                              &
               exc(1,2,2) ,mo_integrals_map)
         endif
       else if (exc(0,1,1) == 2) then
         ! Double alpha
         hij = phase*(get_two_e_integral(                         &
             exc(1,1,1),                                              &
             exc(2,1,1),                                              &
             exc(1,2,1),                                              &
             exc(2,2,1) ,mo_integrals_map) -                          &
             get_two_e_integral(                                  &
             exc(1,1,1),                                              &
             exc(2,1,1),                                              &
             exc(2,2,1),                                              &
             exc(1,2,1) ,mo_integrals_map) )
       else if (exc(0,1,2) == 2) then
         ! Double beta
         hij = phase*(get_two_e_integral(                         &
             exc(1,1,2),                                              &
             exc(2,1,2),                                              &
             exc(1,2,2),                                              &
             exc(2,2,2) ,mo_integrals_map) -                          &
             get_two_e_integral(                                  &
             exc(1,1,2),                                              &
             exc(2,1,2),                                              &
             exc(2,2,2),                                              &
             exc(1,2,2) ,mo_integrals_map) )
       endif
     case (1)
        call get_single_excitation(key_i,key_j,exc,phase,Nint)
        !DIR$ FORCEINLINE
        call bitstring_to_list_ab(key_i, occ, n_occ_ab, Nint)
        ! find the holes and particles from key_i --> key_j 
        if (exc(0,1,1) == 1) then
          ! Single alpha
          h = exc(1,1,1) ! hole
          p = exc(1,2,1) ! particle
          spin = 1
        else
          ! Single beta
          h = exc(1,1,2) ! hole
          p = exc(1,2,2) ! particle
          spin = 2
        endif
        call get_single_excitation_from_fock(key_i,key_j,p,h,spin,phase,hij)
        ! extra contribution from the e-pos interaction 
        hij += phase * eff_2_idx_e_ints(i_pos,h,p)
     case (0)
       hij = diag_H_mat_elem(key_i,Nint) 
       hij += pos_diag_H_mat_elem(key_i, i_pos, Nint)
   end select
  else ! mixed electron/positron excitation 
   if(degree == 1)then ! single excitation electron // single excitation positron 
     call get_single_excitation(key_i,key_j,exc,phase,Nint)
     !DIR$ FORCEINLINE
     call bitstring_to_list_ab(key_i, occ, n_occ_ab, Nint)
     if (exc(0,1,1) == 1) then
       ! Single alpha
       h = exc(1,1,1) ! hole
       p = exc(1,2,1) ! particle
       spin = 1
     else
       ! Single beta
       h = exc(1,1,2) ! hole
       p = exc(1,2,2) ! particle
       spin = 2
     endif
     hij +=  phase * mo_two_e_pos_int(h,i_pos,p,j_pos)  
   elseif(degree == 0)then ! single excitation of the positron 
    hij += pos_single_exc_H_mat_elem(key_i, i_pos, j_pos, Nint)
   endif
  endif
end


double precision function pos_diag_H_mat_elem(key_i, i_pos, Nint)
  BEGIN_DOC
! diagonal element of the Hamiltonian matrix containing all things depending on the positron
  END_DOC
  use bitmasks
  implicit none
  integer(bit_kind), intent(in)  :: key_i(Nint,2)
  integer, intent(in) :: i_pos, Nint
  integer :: occ(Nint*bit_kind_size,2), n_ab(2)
  integer :: ispin, i, i_elec

  ! You get the occupation of electrons in key_i: occ(i) = ith occupation 
  call bitstring_to_list_ab(key_i, occ, n_ab, Nint)
  pos_diag_H_mat_elem = 0.d0
  ! one body term 
  pos_diag_H_mat_elem += mo_pos_integrals(i_pos,i_pos)
  ! interaction of the positron with the electrons 
  do ispin = 1, 2 
   do i = 1, n_ab(ispin) ! n_ab(ispin)  == number of electron of spin 'ispin'
    i_elec = occ(i,ispin) ! index of the orbital occupied by the ith electron  of spin ispin
    pos_diag_H_mat_elem += diag_mo_two_e_pos_int(i_elec,i_pos) ! -phi_i_elec(r1)^2 1/r12 phi_j_pos(r2)^2
   enddo
  enddo

end

double precision function pos_single_exc_H_mat_elem(key_i, i_pos, j_pos, Nint)
  BEGIN_DOC
! single excitation for the Hamiltonian containing the positron 
! the electronic part are therefore the same 
  END_DOC
  use bitmasks
  implicit none
  integer, intent(in)            :: Nint, i_pos, j_pos
  integer(bit_kind), intent(in)  :: key_i(Nint,2)
  integer :: occ(Nint*bit_kind_size,2), n_ab(2)
  integer :: ispin, i, i_elec

  ! You get the occupation of electrons in key_i: occ(i) = ith occupation 
  call bitstring_to_list_ab(key_i, occ, n_ab, Nint)
  pos_single_exc_H_mat_elem = 0.d0

  ! one body term 
  pos_single_exc_H_mat_elem += mo_pos_integrals(i_pos,j_pos)

  ! interaction of the positron with the electrons 
  do ispin = 1, 2 
   do i = 1, n_ab(ispin) ! n_ab(ispin)  == number of electron of spin 'ispin'
    i_elec = occ(i,ispin) ! index of the orbital occupied by the ith electron  of spin ispin
    pos_single_exc_H_mat_elem += eff_2_idx_pos_ints(i_elec, i_pos, j_pos) ! -phi_i_elec(r1)^2 1/r12 phi_i_pos(r2) phi_j_pos(r2)
   enddo
  enddo

end


subroutine i_H_j_single_spin_e_pos(key_i,key_j,Nint,spin,hij)
  use bitmasks
  implicit none
  BEGIN_DOC
  ! Returns $\langle i|H|j \rangle$ where $i$ and $j$ are determinants differing by
  ! a single excitation.
  !
  ! IN THE PRESENCE OF A POSITRON IN THE GROUND STATE ORBITAL 
  END_DOC
  integer, intent(in)            :: Nint, spin
  integer(bit_kind), intent(in)  :: key_i(Nint,2), key_j(Nint,2)
  double precision, intent(out)  :: hij

  integer                        :: exc(0:2,2)
  double precision               :: phase

  PROVIDE big_array_exchange_integrals mo_two_e_integrals_in_map ref_bitmask_energy_e_pos

  call get_single_excitation_spin(key_i(1,spin),key_j(1,spin),exc,phase,Nint)
  call get_single_excitation_from_fock(key_i,key_j,exc(1,1),exc(1,2),spin,phase,hij)
  integer :: i_pos, h, p
  h = exc(1,1)
  p = exc(1,2)
  i_pos = 1
  hij += phase * eff_2_idx_e_ints(i_pos,h,p)
end


