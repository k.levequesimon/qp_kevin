
BEGIN_PROVIDER [ double precision, H_matrix_all_dets_e_pos,(n_det_total_e_pos,n_det_total_e_pos) ]
  use bitmasks
 implicit none
 BEGIN_DOC
 ! |He/pos| matrix on the basis of the Slater determinants / positronic occupation defined by psi_total_e_pos/occ_total_e_pos
 END_DOC
 integer :: i,j,k,i_pos, j_pos
 double precision :: hij
 integer :: degree(n_det_total_e_pos),idx(0:n_det_total_e_pos)
 i_pos = 1
 j_pos = 1
 call  i_H_j_e_pos(psi_total_e_pos(1,1,1),psi_total_e_pos(1,1,1),i_pos,j_pos,N_int,hij)
 !$OMP PARALLEL DO SCHEDULE(GUIDED) DEFAULT(NONE) PRIVATE(i,j,hij,degree,idx,k,i_pos,j_pos) &
 !$OMP SHARED (n_det_total_e_pos, psi_total_e_pos, N_int,H_matrix_all_dets_e_pos,occ_total_e_pos)
 do i =1,n_det_total_e_pos
   i_pos = occ_total_e_pos(i) 
   do j = i, n_det_total_e_pos
    j_pos = occ_total_e_pos(j) 
    call i_H_j_e_pos(psi_total_e_pos(1,1,i),psi_total_e_pos(1,1,j),i_pos, j_pos, N_int,hij)
    H_matrix_all_dets_e_pos(i,j) = hij
    H_matrix_all_dets_e_pos(j,i) = hij
  enddo
 enddo
 !$OMP END PARALLEL DO
END_PROVIDER

BEGIN_PROVIDER [ double precision, diag_H_mat_dets_e_pos, (n_det_total_e_pos)]
 implicit none
 BEGIN_DOC
! <Ielec, i_pos | H |Ielec, i_po> on the basis of the Slater determinants / positronic occupation defined by psi_total_e_pos/occ_total_e_pos
 END_DOC
 use bitmasks
 integer :: i_pos,i
 double precision :: hii
 do i = 1, n_det_total_e_pos
  i_pos = occ_total_e_pos(i)
  call i_H_j_e_pos(psi_total_e_pos(1,1,i),psi_total_e_pos(1,1,i),i_pos, i_pos, N_int,hii)
  diag_H_mat_dets_e_pos(i) = hii
 enddo

END_PROVIDER 

BEGIN_PROVIDER [ double precision, ref_bitmask_energy_e_pos]
 implicit none
 use bitmasks
 integer :: i_pos
 double precision :: hii
 i_pos = 1
 call i_H_j_e_pos(HF_bitmask,HF_bitmask,i_pos, i_pos, N_int,hii)
 ref_bitmask_energy_e_pos = hii

END_PROVIDER 

