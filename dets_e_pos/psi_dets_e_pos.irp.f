
 use bitmasks
 BEGIN_PROVIDER [ integer, index_hf]
&BEGIN_PROVIDER [ integer, n_singles_alpha]
&BEGIN_PROVIDER [ integer, n_singles_beta]
&BEGIN_PROVIDER [ integer, n_doubles_alpha_beta]
&BEGIN_PROVIDER [ integer, n_doubles_alpha_alpha]
&BEGIN_PROVIDER [ integer, n_doubles_beta_beta]
&BEGIN_PROVIDER [ integer, exc_degree_with_HF, (N_det)]
 BEGIN_DOC
! index_hf = index of the HF Slater determinant in psi_det 
!
! n_singles_alpha = Number of single excitations of alpha spin in psi_det 
!
! n_singles_beta = Number of single excitations of beta spin in psi_det 
 END_DOC
 implicit none
 integer :: i,j,degree
 integer :: exc(0:2,2,2),h1,p1,h2,p2,s1,s2
 double precision  :: phase
 n_singles_alpha = 0
 n_singles_beta  = 0
 n_doubles_alpha_beta = 0
 n_doubles_beta_beta = 0
 n_doubles_alpha_alpha = 0
 do i = 1, N_det
  call get_excitation_degree(HF_bitmask,psi_det(1,1,i),degree,N_int) 
  exc_degree_with_HF(i) = degree
  if(degree > 2)cycle
  if(degree==2)then
   call get_double_excitation(HF_bitmask,psi_det(1,1,i),exc,phase,N_int)
   call decode_exc(exc,degree,h1,p1,h2,p2,s1,s2)
   if(s1.ne.s2)then ! double excitaiton alpha/beta
    n_doubles_alpha_beta += 1
   else 
    if(s1 == 1)then ! double alpha alpha 
     n_doubles_alpha_alpha += 1
    else
     n_doubles_beta_beta += 1 ! double alpha alpha 
    endif
   endif
  else if(degree == 0)then
    index_hf = i
  else ! degree == 1
    !  I know that psi_det(1,1,i) is a single excitation with respect to HF 
    call get_single_excitation(HF_bitmask,psi_det(1,1,i),exc,phase,N_int)
    !  
    call decode_exc(exc,degree,h1,p1,h2,p2,s1,s2)
    ! h1 --> p1, s1 |psi_det(1,1,i)> == a^{dagger}_(p1,s1) a_(h1,s1) |HF>
    if(s1 == 1)then ! single excitation alpha 
     n_singles_alpha += 1
    else
     n_singles_beta  += 1
    endif
  endif
 enddo
END_PROVIDER 

 BEGIN_PROVIDER [ integer, n_doubles_in_psi_det]
 implicit none
 n_doubles_in_psi_det = n_doubles_alpha_alpha + n_doubles_beta_beta + n_doubles_alpha_beta
 END_PROVIDER 

 BEGIN_PROVIDER [ integer, list_doubles, (n_doubles_in_psi_det)]
 implicit none
 integer :: i,j
 j = 0
 do i = 1, n_doubles_alpha_beta
  j += 1
  list_doubles(j) = list_double_alpha_beta(i)
 enddo
 do i = 1, n_doubles_alpha_alpha
  j += 1
  list_doubles(j) = list_double_alpha_alpha(i)
 enddo
 do i = 1, n_doubles_beta_beta
  j += 1
  list_doubles(j) = list_double_beta_beta(i)
 enddo
 END_PROVIDER 

 BEGIN_PROVIDER [ integer, list_double_alpha_alpha, (n_doubles_alpha_alpha) ]
&BEGIN_PROVIDER [ integer, list_double_beta_beta, (n_doubles_beta_beta) ]
&BEGIN_PROVIDER [ integer, list_double_alpha_beta, (n_doubles_alpha_beta) ]
 implicit none
 integer :: i,j,degree,i_aa, i_bb, i_ab
 integer :: exc(0:2,2,2),h1,p1,h2,p2,s1,s2
 double precision  :: phase
 i_aa = 0
 i_bb = 0
 i_ab = 0
 do i = 1, N_det
  call get_excitation_degree(HF_bitmask,psi_det(1,1,i),degree,N_int) 
  if(degree .ne. 2)cycle
   call get_double_excitation(HF_bitmask,psi_det(1,1,i),exc,phase,N_int)
   call decode_exc(exc,degree,h1,p1,h2,p2,s1,s2)
   if(s1.ne.s2)then ! double excitaiton alpha/beta
    i_ab += 1
    list_double_alpha_beta(i_ab) = i
   else 
    if(s1 == 1)then ! double alpha alpha 
     i_aa += 1
     list_double_alpha_alpha(i_aa) = i
    else
     i_bb += 1 ! double alpha alpha 
     list_double_beta_beta(i_bb) = i
    endif
   endif
 enddo

 END_PROVIDER 

 BEGIN_PROVIDER [ integer, list_singles_alpha, (n_singles_alpha)]
&BEGIN_PROVIDER [ integer, list_singles_beta, (n_singles_beta)]
! use bitmasks
 BEGIN_DOC
! list of single excitations for alpha/beta spin in psi_det 
 END_DOC
 implicit none
 integer :: i,j,degree,n_a, n_b
 integer :: exc(0:2,2,2),h1,p1,h2,p2,s1,s2
 double precision  :: phase
 n_a = 0
 n_b = 0
 do i = 1, N_det
  call get_excitation_degree(HF_bitmask,psi_det(1,1,i),degree,N_int) 
  if(degree==1)then
   !  I know that psi_det(1,1,i) is a single excitation with respect to HF 
   call get_single_excitation(HF_bitmask,psi_det(1,1,i),exc,phase,N_int)
   call decode_exc(exc,degree,h1,p1,h2,p2,s1,s2)
   ! h1 --> p1, s1 |psi_det(1,1,i)> == a^{dagger}_(p1,s1) a_(h1,s1) |HF>
   if(s1 == 1)then ! single excitation alpha 
    n_a += 1
    list_singles_alpha(n_a) = i
   else
    n_b += 1
    list_singles_beta(n_b) = i
   endif
  endif
 enddo
END_PROVIDER 

BEGIN_PROVIDER [ integer(bit_kind), psi_singles_alpha, (N_int,2,n_singles_alpha)]
! use bitmasks
 BEGIN_DOC
! determinants of single excitations for alpha  ::: contains the information of where are the electrons in what orbitals
 END_DOC
 implicit none
 integer :: i,idx
 do i = 1, n_singles_alpha
  idx = list_singles_alpha(i) ! index of the "ith" single alpha in psi_det
  psi_singles_alpha(:,:,i) = psi_det(:,:,idx)
 enddo
END_PROVIDER 

BEGIN_PROVIDER [ integer(bit_kind), psi_singles_beta, (N_int,2,n_singles_beta)]
! use bitmasks
 BEGIN_DOC
! determinants of single excitations for beta  ::: contains the information of where are the electrons in what orbitals
 END_DOC
 implicit none
 integer :: i,idx
 do i = 1, n_singles_beta
  idx = list_singles_beta(i) ! index of the "ith" single beta in psi_det
  psi_singles_beta(:,:,i) = psi_det(:,:,idx)
 enddo
END_PROVIDER 

 BEGIN_PROVIDER [ integer, n_single_e_plus]
 implicit none
 BEGIN_DOC
! number of single excitations for positron 
 END_DOC
 n_single_e_plus = mo_num - 1
 END_PROVIDER 
  
 BEGIN_PROVIDER [ integer, list_single_e_plus, (n_single_e_plus)]
 implicit none
 BEGIN_DOC
! list of the possible orbitals in which the positron will be excited 
 END_DOC
 integer :: i
 do i = 2, mo_num
  list_single_e_plus(i-1) = i
 enddo
 END_PROVIDER 

 BEGIN_PROVIDER [ integer, n_s_alpha_n_s_p]
  implicit none 
  BEGIN_DOC
! number of double excitations of type :: single alpha * single positron 
  END_DOC
  n_s_alpha_n_s_p = n_singles_alpha * n_single_e_plus
 END_PROVIDER 

 BEGIN_PROVIDER [ integer, n_s_beta_n_s_p]
  implicit none 
  BEGIN_DOC
! number of double excitations of type :: single beta * single positron 
  END_DOC
  n_s_beta_n_s_p = n_singles_beta * n_single_e_plus
 END_PROVIDER 

 BEGIN_PROVIDER [ integer(bit_kind), psi_s_alpha_s_e_plus, (N_int,2,n_s_alpha_n_s_p)]
&BEGIN_PROVIDER [ integer          , occ_s_alpha_s_e_plus, (n_s_alpha_n_s_p)]
! use bitmasks
  BEGIN_DOC
! psi_s_alpha_s_e_plus(:,:,i) === electronic occupation of the ith double excitation of type single alpha * single e+
!
! occ_s_alpha_s_e_plus(    i) === positronic occupation of the ith double excitation of type single alpha * single e+
  END_DOC
 implicit none
 integer :: i,j,icount
 icount = 0
 do i = 1, n_singles_alpha
  ! for each single alpha, we know that we have n_single_e_plus single excitations of the positron 
  do j = 1, n_single_e_plus
   icount += 1
   psi_s_alpha_s_e_plus(:,:,icount) = psi_singles_alpha(:,:,i) ! copy the electronic part 
   occ_s_alpha_s_e_plus(icount) = list_single_e_plus(j)
  enddo
 enddo

 END_PROVIDER 

 BEGIN_PROVIDER [ integer(bit_kind), psi_s_beta_s_e_plus, (N_int,2,n_s_beta_n_s_p)]
&BEGIN_PROVIDER [ integer          , occ_s_beta_s_e_plus, (n_s_beta_n_s_p)        ]
! use bitmasks
  BEGIN_DOC
! psi_s_beta_s_e_plus(:,:,i) === electronic occupation of the ith double excitation of type single beta * single e+
!
! occ_s_beta_s_e_plus(    i) === positronic occupation of the ith double excitation of type single beta * single e+
  END_DOC
 implicit none
 integer :: i,j,icount
 icount = 0
 do i = 1, n_singles_beta
  ! for each single beta, we know that we have n_single_e_plus single excitations of the positron 
  do j = 1, n_single_e_plus
   icount += 1
   psi_s_beta_s_e_plus(:,:,icount) = psi_singles_beta(:,:,i) ! copy the electronic part 
   occ_s_beta_s_e_plus(icount) = list_single_e_plus(j)
  enddo
 enddo

 END_PROVIDER 

 BEGIN_PROVIDER [ integer(bit_kind), psi_HF_e_single_e_plus, (N_int,2,n_single_e_plus)]
&BEGIN_PROVIDER [ integer          , occ_HF_e_single_e_plus, (n_single_e_plus)        ]
! use bitmasks
  BEGIN_DOC
! psi_HF_e_single_e_plus(:,:,i) === electronic occupation of the ith single excitation of type HF(electron) * single e+
!
! occ_HF_e_single_e_plus(    i) === positronic occupation of the ith single excitation of type HF(electron) * single e+
  END_DOC
 implicit none
 integer :: i,j,icount
 do i = 1, n_single_e_plus
  psi_HF_e_single_e_plus(:,:,i) = HF_bitmask(:,:)
  occ_HF_e_single_e_plus(i) = list_single_e_plus(i)
 enddo
 END_PROVIDER 

!!!! I removed that provider because it is useless to copy the determinants in psi_det twice 
! BEGIN_PROVIDER [ integer(bit_kind), psi_elec_zero_exc_pos, (N_int,2,N_det) ]
!&BEGIN_PROVIDER [ integer          , occ_elec_zero_exc_pos, (N_det) ]
! use bitmasks
! BEGIN_DOC
! psi_elec_zero_exc_pos(:,:,i) === electronic occupation of the ith HF/single/double purely electronic "excitation" with positron in ground state 
!
! occ_elec_zero_exc_pos(i)     === positronic occupation of the ith HF/single/double purely electronic "excitation" with positron in ground state 
! END_DOC
! implicit none
! integer :: i
! do i = 1, N_det
!  psi_elec_zero_exc_pos(:,:,i) = psi_det(:,:,i)
!  occ_elec_zero_exc_pos(i) = 1
! enddo
! END_PROVIDER 

 BEGIN_PROVIDER [ integer, n_det_total_e_pos]
 implicit none
 if(cisdt_e_pos)then
 !                    zero excitation sector  + HF(e)*Single(e+) + S(alpha)*S(e+)  + S(beta)*S(e+) + D(e)*S(e+)
  n_det_total_e_pos = N_det                    + n_single_e_plus  + n_s_alpha_n_s_p + n_s_beta_n_s_p & 
                    + n_doubles_in_psi_det * n_single_e_plus
 else if(cis_e_pos)then
 !                    zero excitation sector  + HF(e)*Single(e+) 
  n_det_total_e_pos = N_det                    + n_single_e_plus 
 else
 !                    zero excitation sector  + HF(e)*Single(e+) + S(alpha)*S(e+)  + S(beta)*S(e+)
  n_det_total_e_pos = N_det                    + n_single_e_plus  + n_s_alpha_n_s_p + n_s_beta_n_s_p
 endif
 print*,'n_det_total_e_pos ',n_det_total_e_pos
 print*,'n_single_e_plus = ',n_single_e_plus
 print*,'n_s_alpha_n_s_p = ',n_s_alpha_n_s_p
 print*,'n_s_beta_n_s_p  = ',n_s_beta_n_s_p
 print*,'N_det = ',N_det

 END_PROVIDER 

 BEGIN_PROVIDER [ integer, degree_exc_pos, (n_det_total_e_pos)]
&BEGIN_PROVIDER [ integer, degree_exc_total_e_pos, (n_det_total_e_pos)]
&BEGIN_PROVIDER [ integer, spin_exc_total_e_pos, (2,n_det_total_e_pos)]
&BEGIN_PROVIDER [ integer, hp_total_e_pos, (4,n_det_total_e_pos)]
&BEGIN_PROVIDER [ integer(bit_kind), hp_total_e_pos_det, (2,N_int,n_det_total_e_pos)]
 implicit none
 integer           :: exc(0:2,2,2)
 integer           :: j,h1,p1,s1,h2,p2,s2,degree     
 double precision  :: phase
 do j  = 1, n_det_total_e_pos
  if(occ_total_e_pos(j).ne.0)then
   degree_exc_pos(j) = 1
  else 
   degree_exc_pos(j) = 0
  endif
  call get_excitation_degree(HF_bitmask,psi_total_e_pos(1,1,j),degree,N_int)
  call get_excitation(HF_bitmask,psi_total_e_pos(1,1,j),exc,degree,phase,N_int)
  if(degree==0)then
   degree_exc_total_e_pos(j) = degree
   spin_exc_total_e_pos(1,j) = 0
   spin_exc_total_e_pos(2,j) = 0
   hp_total_e_pos(1,j) = 0 
   hp_total_e_pos(2,j) = 0 
   hp_total_e_pos(3,j) = 0 
   hp_total_e_pos(4,j) = 0 
  else
   call decode_exc(exc,degree,h1,p1,h2,p2,s1,s2)
   degree_exc_total_e_pos(j) = degree
   spin_exc_total_e_pos(1,j) = s1
   spin_exc_total_e_pos(2,j) = s2
   hp_total_e_pos(1,j) = h1
   hp_total_e_pos(2,j) = p1
   hp_total_e_pos(3,j) = h2
   hp_total_e_pos(4,j) = p2
  endif
 enddo
END_PROVIDER 

 BEGIN_PROVIDER [ integer(bit_kind), psi_total_e_pos, (N_int,2,n_det_total_e_pos) ]
&BEGIN_PROVIDER [ integer          , occ_total_e_pos, (n_det_total_e_pos) ]
&BEGIN_PROVIDER [ integer          , list_single_e_plus_cisd, (n_single_e_plus) ]
&BEGIN_PROVIDER [ integer          , list_single_e_plus_single_alpha_cisd, (n_s_alpha_n_s_p) ]
&BEGIN_PROVIDER [ integer          , list_single_e_plus_single_beta_cisd,  (n_s_beta_n_s_p) ]
 implicit none
 BEGIN_DOC
 ! total list of Slater determinants and positronic occupations 
 !
 ! Here the first N_det determinants correspond to the CISD wave function in psi_det 
 !
 !                with an occupation of the positron in its ground state orbital   
 !
 ! Then comes the HF electronic with the single excitations of the positron 
 !
 ! Then comes the Singly excited alpha determinants with the single excitations of the positron 
 !
 ! Then comes the Singly excited beta determinants with the single excitations of the positron 
 END_DOC
 integer :: j,i,i_det,ii,i_occ
 integer                        :: exc(0:2,2,2)
 double precision               :: phase
 integer                        :: h2,p2,s2,degree,h1,p1,s1
!use bitmasks
 j = 0
 ! I start by filling psi_total_e_pos/occ_total_e_pos by the zeroth excitation sector 
 do i = 1, N_det
  j += 1
  psi_total_e_pos(:,:,j) = psi_det(:,:,i)
  occ_total_e_pos(j) = 1 
 enddo

 ! HF(elec) * S(e+)
 do i = 1, n_single_e_plus
  j += 1
  psi_total_e_pos(:,:,j) = psi_HF_e_single_e_plus(:,:,i)
  occ_total_e_pos(j) = occ_HF_e_single_e_plus(i)
  list_single_e_plus_cisd(i) = j
 enddo

 if(.not.cis_e_pos)then ! you limit yourself to single excitations
    ! S(alpha) * S(e+)
    do i = 1, n_s_alpha_n_s_p
     j += 1
     psi_total_e_pos(:,:,j) = psi_s_alpha_s_e_plus(:,:,i)
     occ_total_e_pos(j) = occ_s_alpha_s_e_plus(i)
     list_single_e_plus_single_alpha_cisd(i) = j
    enddo
   
    ! S(beta) * S(e+)
    do i = 1, n_s_beta_n_s_p
     j += 1
     psi_total_e_pos(:,:,j) = psi_s_beta_s_e_plus(:,:,i)
     occ_total_e_pos(j) = occ_s_beta_s_e_plus(i)
     list_single_e_plus_single_beta_cisd(i) = j
    enddo
     if(cisdt_e_pos)then
!!!!!  ! D(alpha/alpha)* S(e+)
      do i = 1, n_doubles_alpha_alpha 
       i_det = list_double_alpha_alpha(i)
       do ii = 1, n_single_e_plus
        j += 1
        i_occ = list_single_e_plus(ii)
        psi_total_e_pos(:,:,j) = psi_det(:,:,i_det)
        occ_total_e_pos(j) = i_occ
       enddo
      enddo
!!!!!  ! D(alpha/alpha)* S(e+)
      do i = 1, n_doubles_beta_beta 
       i_det = list_double_beta_beta(i)
       do ii = 1, n_single_e_plus
        j += 1
        i_occ = list_single_e_plus(ii)
        psi_total_e_pos(:,:,j) = psi_det(:,:,i_det)
        occ_total_e_pos(j) = i_occ
       enddo
      enddo
!!!!!  ! D(alpha/beta)* S(e+)
      do i = 1, n_doubles_alpha_beta 
       i_det = list_double_alpha_beta(i)
       do ii = 1, n_single_e_plus
        j += 1
        i_occ = list_single_e_plus(ii)
        psi_total_e_pos(:,:,j) = psi_det(:,:,i_det)
        occ_total_e_pos(j) = i_occ
       enddo
      enddo
     endif
  endif ! if not cis_e_pos
 END_PROVIDER 


