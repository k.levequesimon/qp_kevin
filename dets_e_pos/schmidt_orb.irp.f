program schmidt_orb
 implicit none
 call schmidt_mo
 call schmidt_mo_pos

end

subroutine schmidt_mo
 implicit none
 integer :: i,j,i_mo
 double precision, allocatable :: mo_coef_tmp(:,:),overlap(:,:)
 allocate(mo_coef_tmp(ao_num, mo_num),overlap(mo_num, mo_num))
 mo_coef_tmp = mo_coef
 i_mo = mo_num
 print*,'Gram-Schmidt orthgonalization for electron orbitals '
 print*,'mo_overlap(i_mo,i_mo) = ',mo_overlap(i_mo,i_mo)
 call normalize_mo(mo_coef_tmp, i_mo)
 call compute_overlap(mo_coef_tmp,overlap)
 print*,'overlap before '
 do i = 1, mo_num
  write(*,'(100(F10.7,X))')overlap(:,i)
 enddo
 do i = 1, mo_num-1
  print*,' i ',i
  call remove_overlap_from_orb(mo_coef_tmp,mo_coef(1,i),i_mo)
  call normalize_mo(mo_coef_tmp, i_mo)
 enddo
 call compute_overlap(mo_coef_tmp,overlap)
 print*,'overlap after'
 do i = 1, mo_num
  write(*,'(100(F10.7,X))')overlap(:,i)
 enddo
 mo_coef = mo_coef_tmp
 touch mo_coef
 call save_mos
end


subroutine schmidt_mo_pos
 implicit none
 integer :: i,j,i_mo
 double precision, allocatable :: mo_coef_pos_tmp(:,:),overlap(:,:)
 allocate(mo_coef_pos_tmp(ao_num, mo_num),overlap(mo_num, mo_num))
 mo_coef_pos_tmp = mo_coef_pos
 i_mo = mo_num
 print*,'Gram-Schmidt orthgonalization for positron orbitals '
 print*,'mo_overlap(i_mo,i_mo) = ',mo_overlap(i_mo,i_mo)
 call normalize_mo(mo_coef_pos_tmp, i_mo)
 call compute_overlap(mo_coef_pos_tmp,overlap)
 print*,'overlap before '
 do i = 1, mo_num
  write(*,'(100(F10.7,X))')overlap(:,i)
 enddo
 do i = 1, mo_num-1
  print*,' i ',i
  call remove_overlap_from_orb(mo_coef_pos_tmp,mo_coef_pos(1,i),i_mo)
  call normalize_mo(mo_coef_pos_tmp, i_mo)
 enddo
 call compute_overlap(mo_coef_pos_tmp,overlap)
 print*,'overlap after'
 do i = 1, mo_num
  write(*,'(100(F10.7,X))')overlap(:,i)
 enddo
 mo_coef_pos = mo_coef_pos_tmp
 touch mo_coef_pos
 call save_mos_pos
end


subroutine normalize_mo(mo_coef_tmp, i_mo)
 implicit none
 double precision, intent(inout) :: mo_coef_tmp(ao_num, mo_num)
 integer, intent(in) :: i_mo
 double precision :: accu
 integer :: m,n
 accu = 0.d0
 do m = 1, ao_num
  do n = 1, ao_num
   accu += mo_coef_tmp(n,i_mo) * mo_coef_tmp(m,i_mo) * ao_overlap(n,m)
  enddo
 enddo
 print*,'norm of i_mo before = ',accu
 accu = 1.d0/dsqrt(accu)
 do m = 1, ao_num
  mo_coef_tmp(m,i_mo) *= accu
 enddo
 accu = 0.d0
 do m = 1, ao_num
  do n = 1, ao_num
   accu += mo_coef_tmp(n,i_mo) * mo_coef_tmp(m,i_mo) * ao_overlap(n,m)
  enddo
 enddo
 print*,'norm of i_mo after  = ',accu
end

subroutine remove_overlap_from_orb(mo_coef_tmp,mo_to_remove,i_mo)
 implicit none
 double precision, intent(inout) :: mo_coef_tmp(ao_num, mo_num)
 double precision, intent(in)    :: mo_to_remove(ao_num)
 integer, intent(in) :: i_mo
 double precision :: accu,norm_to_remove
 integer :: m,n
 accu = 0.d0
 norm_to_remove = 0.d0
 do m = 1, ao_num
  do n = 1, ao_num
   accu += mo_coef_tmp(n,i_mo) * mo_to_remove(m) * ao_overlap(n,m)
   norm_to_remove += mo_to_remove(n) * mo_to_remove(m) * ao_overlap(n,m)
  enddo
 enddo
 print*,'norm_to_remove                = ',norm_to_remove
 print*,'overlap old with mo_to_remove = ',accu
 do m = 1, ao_num
  mo_coef_tmp(m,i_mo) -= mo_to_remove(m) * accu / norm_to_remove
 enddo
 accu = 0.d0
 do m = 1, ao_num
  do n = 1, ao_num
   accu += mo_coef_tmp(n,i_mo) * mo_to_remove(m) * ao_overlap(n,m)
  enddo
 enddo
 print*,'overlap new with mo_to_remove = ',accu
end

subroutine compute_overlap(mo_coef_tmp,overlap)
 implicit none
 double precision, intent(in)  :: mo_coef_tmp(ao_num, mo_num)
 double precision, intent(out) :: overlap(mo_num, mo_num)
 double precision :: accu_d,accu_nd
 integer :: i,j,m,n
 overlap = 0.d0
 accu_d = 0.d0
 accu_nd = 0.d0
 do i = 1, mo_num
  do j = 1, mo_num
   do m = 1, ao_num
    do n = 1, ao_num
     overlap(j,i) += ao_overlap(n,m) * mo_coef_tmp(n,j) * mo_coef_tmp(m,i)
    enddo
   enddo
  enddo
 enddo

 accu_d = 0.d0
 accu_nd = 0.d0
 do i = 1, mo_num
  accu_d += dabs(overlap(i,i))
  do j = 1, mo_num
   if(i==j)cycle
   accu_nd += dabs(overlap(j,i))
  enddo
 enddo
 accu_d = accu_d/dble(mo_num)
 accu_nd = accu_nd/dble(mo_num*mo_num)
 print*,'accu_d = ',accu_d
 print*,'accu_nd= ',accu_nd
end
