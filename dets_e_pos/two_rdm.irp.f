BEGIN_PROVIDER [ double precision, mo_two_rdm_e_pos, (mo_num, mo_num, mo_num, mo_num,n_states)]
 implicit none
 BEGIN_DOC
 ! mo_two_rdm_e_pos(k,i,l,j,istate) = <Psi^{ep}| a^dagger_[k,e] a_[i,e] b^dagger_[l,p] b_[j,p] |Psi^{ep}>
 END_DOC
 use bitmasks ! you need to include the bitmasks_module.f90 features
 integer :: i,degree_pos_i,i_pos,degree_elec_i,degree_tot_i,i_occ_e,l
 integer :: j,degree_pos_j,j_pos,degree_elec_j,degree_tot_j,k,degree_pos
 integer, allocatable           :: occ(:,:)
 integer                        :: n_occ_ab(2),ispin
 integer                        :: exc(0:2,2,2)
 double precision               :: phase
 integer                        :: h2,p2,s2,degree_elec,h1,p1,s1
 allocate(occ(N_int*bit_kind_size,2))
 mo_two_rdm_e_pos = 0.d0
 do i=1,N_e_pos_det
   i_pos=pos_occ(i)
   degree_elec_i = degree_exc_total_e_pos_general(i)
   degree_pos_i  = degree_exc_pos_general(i)
   degree_tot_i = degree_elec_i + degree_pos_i 
   ! diagonal term 
   call bitstring_to_list_ab(psi_e_pos_det(1,1,i), occ, n_occ_ab, N_int)
   do k = 1,n_states
    do ispin= 1,2
     do l = 1, n_occ_ab(ispin) ! browsing the alpha electrons
       i_occ_e = occ(l,ispin)
       mo_two_rdm_e_pos(i_occ_e,i_occ_e,i_pos,i_pos,k) += psi_e_pos_coef(i,k)*psi_e_pos_coef(i,k)
     enddo
    enddo
   enddo
   do j=i+1,N_e_pos_det ! extra diagonal term
    j_pos=pos_occ(j)
    degree_elec_j = degree_exc_total_e_pos_general(j)
    degree_pos_j  = degree_exc_pos_general(j)
    degree_tot_j = degree_elec_j + degree_pos_j 
    if(abs(degree_pos_i-degree_pos_j).gt.1)cycle
    if(abs(degree_elec_i-degree_elec_j).gt.1)cycle
    call get_excitation(psi_e_pos_det(1,1,i),psi_e_pos_det(1,1,j),exc,degree_elec,phase,N_int)
    if(degree_elec==1)then ! single excitation between electrons 
     call decode_exc(exc,degree_elec,h1,p1,h2,p2,s1,s2)
     do k = 1, N_states
      mo_two_rdm_e_pos(h1,p1,i_pos,j_pos,k) += 0.5d0 * psi_e_pos_coef(i,k)*psi_e_pos_coef(j,k)*phase
      mo_two_rdm_e_pos(h1,p1,j_pos,i_pos,k) += 0.5d0 * psi_e_pos_coef(i,k)*psi_e_pos_coef(j,k)*phase
      mo_two_rdm_e_pos(p1,h1,i_pos,j_pos,k) += 0.5d0 * psi_e_pos_coef(i,k)*psi_e_pos_coef(j,k)*phase
      mo_two_rdm_e_pos(p1,h1,j_pos,i_pos,k) += 0.5d0 * psi_e_pos_coef(i,k)*psi_e_pos_coef(j,k)*phase
     enddo
    else if(degree_elec == 0)then ! single excitation between positrons
     do k = 1, N_states ! the electrons are the same so diagonal part for the electrons
      do ispin= 1,2
       do l = 1, n_occ_ab(ispin) ! browsing the alpha electrons
         i_occ_e = occ(l,ispin)
         mo_two_rdm_e_pos(i_occ_e,i_occ_e,j_pos,i_pos,k) += 1.0d0 * psi_e_pos_coef(i,k)*psi_e_pos_coef(j,k)
         mo_two_rdm_e_pos(i_occ_e,i_occ_e,i_pos,j_pos,k) += 1.0d0 * psi_e_pos_coef(i,k)*psi_e_pos_coef(j,k)
       enddo
      enddo
     enddo
    endif
   enddo
 enddo

END_PROVIDER 

BEGIN_PROVIDER [ double precision, ints_on_top_pair, (mo_num, mo_num, mo_num, mo_num)]
 implicit none
 integer :: i_e, k_e, j_pos, l_pos , i
 double precision :: weight, r(3), mo_ie, mo_ke, mo_lpos, mo_jpos
 do j_pos = 1, mo_num
  do l_pos = 1, mo_num
   do i_e = 1, mo_num
    do k_e = 1, mo_num
     ints_on_top_pair(k_e, i_e, l_pos, j_pos) = 0.d0
     do i = 1,  n_points_final_grid
      weight = final_weight_at_r_vector(i)
      mo_ie = mos_in_r_array_transp(i,i_e)
      mo_ke = mos_in_r_array_transp(i,k_e)
      mo_lpos = mos_pos_in_r_array_transp(i,l_pos)
      mo_jpos = mos_pos_in_r_array_transp(i,j_pos)
      ints_on_top_pair(k_e, i_e, l_pos, j_pos) += weight * mo_ie * mo_ke * mo_lpos * mo_jpos
     enddo
    enddo
   enddo
  enddo
 enddo
END_PROVIDER 

BEGIN_PROVIDER [ double precision, on_top_e_pos, (N_states)]
 implicit none
 integer :: i_e,j_pos,k_e,l_pos,i_state
 on_top_e_pos = 0.d0
 do i_state = 1,N_states
  do j_pos = 1, mo_num
   do l_pos = 1, mo_num
    do i_e = 1, mo_num
     do k_e = 1, mo_num
      on_top_e_pos(i_state) += mo_two_rdm_e_pos(k_e,i_e,l_pos,j_pos,i_state) * ints_on_top_pair(k_e,i_e,l_pos,j_pos)
     enddo
    enddo
   enddo
  enddo
 enddo
END_PROVIDER 
