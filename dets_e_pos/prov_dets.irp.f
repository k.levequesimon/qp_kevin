use bitmasks
!!! General structure for the providers of the electronic/positronic wave function 
!!! These variables are stored in the EZFIO and therefore can be written and read 
BEGIN_PROVIDER [ integer, N_e_pos_det ]
  implicit none
  BEGIN_DOC
  ! Number of determinants in the wave function
  END_DOC
  logical                        :: exists
  character*(64)                 :: label
  PROVIDE read_wf ezfio_filename nproc
  if (mpi_master) then
    if (read_wf) then
      call ezfio_has_dets_e_pos_N_e_pos_det(exists)
      if (exists) then
        call ezfio_get_dets_e_pos_N_e_pos_det(N_e_pos_det)
      else
        N_e_pos_det = 1
      endif
    else
      N_e_pos_det = 1
    endif
    call write_int(6,N_e_pos_det,'Number of electron/positron determinants')
  endif
  IRP_IF MPI_DEBUG
    print *,  irp_here, mpi_rank
    call MPI_BARRIER(MPI_COMM_WORLD, ierr)
  IRP_ENDIF
  IRP_IF MPI
    include 'mpif.h'
    integer                        :: ierr
    call MPI_BCAST( N_e_pos_det, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierr)
    if (ierr /= MPI_SUCCESS) then
      stop 'Unable to read N_e_pos_det with MPI'
    endif
  IRP_ENDIF

  ASSERT (N_e_pos_det > 0)
END_PROVIDER


BEGIN_PROVIDER [ integer(bit_kind), psi_e_pos_det, (N_int,2,N_e_pos_det) ]
  implicit none
  BEGIN_DOC
  ! The determinants of the wave function. Initialized with Hartree-Fock if the |EZFIO| file
  ! is empty.
  END_DOC
  integer                        :: i
  logical                        :: exists
  character*(64)                 :: label

  PROVIDE read_wf N_e_pos_det ezfio_filename HF_bitmask 
  psi_e_pos_det = 0_bit_kind
  if (mpi_master) then
    if (read_wf) then
      call ezfio_has_determinants_N_int(exists)
      if (exists) then
        call ezfio_has_determinants_bit_kind(exists)
        if (exists) then
          call ezfio_has_dets_e_pos_N_e_pos_det(exists)
          if (exists) then
            call ezfio_has_determinants_N_states(exists)
            if (exists) then
              call ezfio_has_dets_e_pos_psi_e_pos_det(exists)
            endif
          endif
        endif
      endif
    endif
  endif

  if (exists) then
    call read_dets_e_pos(psi_e_pos_det,N_int,N_e_pos_det)
    print *,  'Read psi_e_pos_det'
  else
    print*,'Initializing with HF '
    psi_e_pos_det = 0_bit_kind
    do i=1,N_int
      psi_e_pos_det(i,1,1) = HF_bitmask(i,1)
      psi_e_pos_det(i,2,1) = HF_bitmask(i,2)
    enddo
  endif

  IRP_IF MPI_DEBUG
    print *,  irp_here, mpi_rank
    call MPI_BARRIER(MPI_COMM_WORLD, ierr)
  IRP_ENDIF
  IRP_IF MPI
    include 'mpif.h'
    integer                        :: ierr
    call     MPI_BCAST( psi_e_pos_det, N_int*2*N_e_pos_det, MPI_BIT_KIND, 0, MPI_COMM_WORLD, ierr)
    if (ierr /= MPI_SUCCESS) then
      stop 'Unable to read psi_e_pos_det with MPI'
    endif
  IRP_ENDIF


END_PROVIDER



BEGIN_PROVIDER [ double precision, psi_e_pos_coef, (N_e_pos_det,N_states) ]
  implicit none
  BEGIN_DOC
  ! The wave function coefficients. Initialized with Hartree-Fock if the |EZFIO| file
  ! is empty.
  END_DOC

  integer                        :: i,k, N_int2
  logical                        :: exists
  character*(64)                 :: label

  PROVIDE read_wf N_e_pos_det mo_label ezfio_filename
  psi_e_pos_coef = 0.d0
  do i=1,min(N_states,N_e_pos_det)
    psi_e_pos_coef(i,i) = 1.d0
  enddo

  if (mpi_master) then
    if (read_wf) then
      call ezfio_has_dets_e_pos_psi_e_pos_coef(exists)
      if (exists) then

        double precision, allocatable  :: psi_e_pos_coef_read(:,:)
        allocate (psi_e_pos_coef_read(N_e_pos_det,N_states))
        print *,  'Read psi_e_pos_coef', N_e_pos_det, N_states
        call ezfio_get_dets_e_pos_psi_e_pos_coef(psi_e_pos_coef_read)
        do k=1,N_states
          do i=1,N_e_pos_det
            psi_e_pos_coef(i,k) = psi_e_pos_coef_read(i,k)
          enddo
        enddo
        deallocate(psi_e_pos_coef_read)

      endif
    endif
  endif
  IRP_IF MPI_DEBUG
    print *,  irp_here, mpi_rank
    call MPI_BARRIER(MPI_COMM_WORLD, ierr)
  IRP_ENDIF
  IRP_IF MPI
    include 'mpif.h'
    integer                        :: ierr
    call     MPI_BCAST( psi_e_pos_coef, size(psi_e_pos_coef), MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    if (ierr /= MPI_SUCCESS) then
      stop 'Unable to read psi_e_pos_coef with MPI'
    endif
  IRP_ENDIF



END_PROVIDER

BEGIN_PROVIDER [ integer, pos_occ, (N_e_pos_det)]
 implicit none
 logical :: exists
  PROVIDE read_wf N_e_pos_det ezfio_filename 
  pos_occ = 0_bit_kind
  if (mpi_master) then
    call ezfio_has_dets_e_pos_N_e_pos_det(exists)
    if (exists) then
      call ezfio_has_dets_e_pos_pos_occ(exists)
    endif
  endif

 if (exists) then
  print *,  'Read pos_occ'
  call ezfio_get_dets_e_pos_pos_occ(pos_occ)
 else
  pos_occ(1) = 1
 endif
END_PROVIDER 


 BEGIN_PROVIDER [ integer, degree_exc_pos_general, (N_e_pos_det)]
&BEGIN_PROVIDER [ integer, degree_exc_total_e_pos_general, (N_e_pos_det)]
&BEGIN_PROVIDER [ integer, spin_exc_total_e_pos_general, (2,N_e_pos_det)]
&BEGIN_PROVIDER [ integer, hp_total_e_pos_general, (4,N_e_pos_det)]
 implicit none
 integer           :: exc(0:2,2,2)
 integer           :: j,h1,p1,s1,h2,p2,s2,degree,pos_j
 double precision  :: phase
 do j  = 1, N_e_pos_det
  call get_excitation_degree(HF_bitmask,psi_e_pos_det(1,1,j),degree,N_int)
  degree_exc_total_e_pos_general(j) = degree
  pos_j = pos_occ(j)
  if(pos_j .ne.1)then
   degree_exc_pos_general(j) = 1
  else
   degree_exc_pos_general(j) = 0
  endif
  if(degree==0)then
   spin_exc_total_e_pos_general(1,j) = 0
   spin_exc_total_e_pos_general(2,j) = 0
  else if(degree.le.2)then
   call get_excitation(HF_bitmask,psi_e_pos_det(1,1,j),exc,degree,phase,N_int)
   call decode_exc(exc,degree,h1,p1,h2,p2,s1,s2)
   degree_exc_total_e_pos_general(j) = degree
   spin_exc_total_e_pos_general(1,j) = s1
   spin_exc_total_e_pos_general(2,j) = s2
  endif
 enddo
END_PROVIDER 

