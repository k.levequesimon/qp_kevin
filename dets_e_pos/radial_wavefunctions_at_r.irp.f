subroutine radial_wavefunctions_at_r(method,k,str,mos_at_r,output)
  implicit none
  character(len=2), intent(in)  :: method,str
  double precision, intent(in)  :: mos_at_r
  integer, intent(in)           :: k
  double precision, intent(out) :: output
  double precision              :: pi,vec_mos(3),cart_from_mos(3,3),vec_cart(3)
  integer                       :: i,idex(3),mos_2p(3),mos_3p(3)
  parameter (pi=3.14159265358979d0)
  dimension mos_at_r(mo_num)
  data mos_2p/3,4,5/,mos_3p/7,8,9/
  if ((str.eq."2p").or.(str.eq."3p")) then
    if (str.eq."2p") idex=mos_2p
    if (str.eq."3p") idex=mos_3p
    do i=1,3
      if (method.eq."HF") then 
        vec_mos(i)=mos_at_r(idex(i))
        cart_from_mos(:,i)=overlap_mos_cart_HF(:,idex(i))
      else if (method.eq."CI") then 
        vec_mos(i)=dot_product(rdm_elec_in_e_pos_eigvectors_sorted(:,idex(i),k),mos_at_r) 
        cart_from_mos(:,i)=overlap_mos_cart_CI(:,idex(i),k)
      end if
    end do
    vec_cart=matmul(cart_from_mos,vec_mos)
    output=vec_cart(3)*dsqrt((4.d0*pi)/3.d0)
  else 
    if (str.eq."1s") then
      if (method.eq."HF") output=mos_at_r(1)
      if (method.eq."CI") output=dot_product(rdm_elec_in_e_pos_eigvectors_sorted(:,1,k),mos_at_r) 
    else if (str.eq."2s") then
      if (method.eq."HF") output=mos_at_r(2)  
      if (method.eq."CI") output=dot_product(rdm_elec_in_e_pos_eigvectors_sorted(:,2,k),mos_at_r) 
    else if (str.eq."3s") then 
      if (method.eq."HF") output=mos_at_r(6)  
      if (method.eq."CI") output=dot_product(rdm_elec_in_e_pos_eigvectors_sorted(:,6,k),mos_at_r) 
    end if  
    output=output*dsqrt(4.d0*pi)
  end if
end subroutine radial_wavefunctions_at_r

subroutine radial_wavefunctions_pos_at_r(method,str,mos_at_r,output)
  implicit none
  character(len=2), intent(in)  :: method,str
  double precision, intent(in)  :: mos_at_r
  double precision, intent(out) :: output
  double precision              :: pi,vec_mos(3),cart_from_mos(3,3),vec_cart(3)
  integer                       :: i
  parameter (pi=3.14159265358979d0)
  dimension mos_at_r(mo_num)
  if (str.eq."1s") then
    if (method.eq."HF") output=mos_at_r(1)
    if (method.eq."CI") output=dot_product(rdm_pos_in_e_pos_eigvectors(:,mo_num,1),mos_at_r) 
    output=output*dsqrt(4.d0*pi)
  else if (str.eq."2s") then
    if (method.eq."HF") output=mos_at_r(6)
    if (method.eq."CI") output=dot_product(rdm_pos_in_e_pos_eigvectors(:,mo_num,6),mos_at_r) 
    output=output*dsqrt(4.d0*pi)
  else if (str.eq."2p") then
    do i=1,3
      if (method.eq."CI") then 
        vec_mos(i)=dot_product(rdm_pos_in_e_pos_eigvectors(:,mo_num,i+1),mos_at_r) 
        cart_from_mos(:,i)=overlap_mos_pos_cart_CI(:,i+1)
      end if
    end do
    vec_cart=matmul(cart_from_mos,vec_mos)
    output=vec_cart(3)*dsqrt((4.d0*pi)/3.d0)
  end if
end subroutine radial_wavefunctions_pos_at_r
