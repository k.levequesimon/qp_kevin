subroutine save_natorb_elec_in_e_pos
 implicit none
 integer :: i,j
 j=0
 do i = mo_num, 1, -1
  j+= 1
  mo_coef(1:ao_num, j) = rdm_elec_in_e_pos_eigvectors_ao(1:ao_num,i,1)
 enddo
 touch mo_coef
 print*,'saving natorb for electrons '
 call save_mos
end

subroutine save_natorb_pos_in_e_pos
 implicit none
 integer :: i,j
 j=0
 do i = mo_num, 1, -1
  j+= 1
  mo_coef_pos(1:ao_num, j) = rdm_pos_in_e_pos_eigvectors_ao(1:ao_num,i,1)
 enddo
 touch mo_coef_pos
 print*,'saving natorb for positron '
 call save_mos_pos
end

 BEGIN_PROVIDER [double precision,rdm_elec_in_e_pos_eigvalues,(mo_num,n_states)]
&BEGIN_PROVIDER [double precision,rdm_elec_in_e_pos_eigvectors,(mo_num,mo_num,n_states)]
  implicit none
  integer                        :: k,i
  double precision, allocatable  :: tab(:,:)
  allocate(tab(mo_num,mo_num))
  print*,'*********** ELECTRON RDM PROPERTIES ************'
  do k=1,n_states
    print*,'State ',k
    tab(:,:)=rdm_elec_in_e_pos_full_mo(:,:,k)
    call lapack_diag(rdm_elec_in_e_pos_eigvalues(1,k),rdm_elec_in_e_pos_eigvectors(1,1,k),tab,mo_num,mo_num)
    print*,'Electron natural occupation numbers '
    do i = 1, mo_num
     print*,i,rdm_elec_in_e_pos_eigvalues(i,k)
    enddo
  end do
  deallocate(tab)
END_PROVIDER

 BEGIN_PROVIDER [ double precision, rdm_elec_in_e_pos_eigvectors_ao, (ao_num, mo_num, n_states)]
 implicit none
 integer :: k,i,j,m
 rdm_elec_in_e_pos_eigvectors_ao = 0.d0
 do k = 1, n_states
  do i = 1, mo_num
   do j = 1, mo_num
    do m = 1, ao_num
     rdm_elec_in_e_pos_eigvectors_ao(m,i,k) += mo_coef(m,j) * rdm_elec_in_e_pos_eigvectors(j,i,k)
    enddo
   enddo
  enddo
 enddo
 print*,'provided rdm_elec_in_e_pos_eigvectors_ao'
 END_PROVIDER 

 BEGIN_PROVIDER [double precision,rdm_pos_in_e_pos_eigvalues,(mo_num,n_states)]
&BEGIN_PROVIDER [double precision,rdm_pos_in_e_pos_eigvectors,(mo_num,mo_num,n_states)]
&BEGIN_PROVIDER [integer, n_good_rdm_vec, (n_states)]
  implicit none
  integer                        :: k,i
  double precision, allocatable  :: tab(:,:)
  double precision :: thr,accu
 
  thr = 0.99d0
  print*,'*********** POSITRON RDM PROPERTIES ************'
  allocate(tab(mo_num,mo_num))
  do k=1,n_states
    print*,'State ',k
    tab(:,:)=rdm_pos_in_e_pos_mo(:,:,k)
    call lapack_diag(rdm_pos_in_e_pos_eigvalues(1,k),rdm_pos_in_e_pos_eigvectors(1,1,k),tab,mo_num,mo_num)
    print*,'Electron natural occupation numbers '
    accu = 0.d0
    n_good_rdm_vec(k) = 0
    do i = mo_num,-1,1
     if(accu.lt.thr)then
      n_good_rdm_vec(k) +=1
     endif
     accu += rdm_pos_in_e_pos_eigvalues(i,k)
     print*,i,rdm_pos_in_e_pos_eigvalues(i,k)
    enddo
  end do
  deallocate(tab)
END_PROVIDER
 BEGIN_PROVIDER [ double precision, rdm_pos_in_e_pos_eigvectors_ao, (ao_num, mo_num, n_states)]
 implicit none
 integer :: k,i,j,m
 rdm_pos_in_e_pos_eigvectors_ao = 0.d0
 do k = 1, n_states
  do i = 1, mo_num
   do j = 1, mo_num
    do m = 1, ao_num
     rdm_pos_in_e_pos_eigvectors_ao(m,i,k) += mo_coef_pos(m,j) * rdm_pos_in_e_pos_eigvectors(j,i,k)
    enddo
   enddo
  enddo
 enddo
 END_PROVIDER 

 BEGIN_PROVIDER [ double precision, overlap_rdm_pos_in_e_pos_eigvectors, (mo_num, mo_num, n_states)]
 implicit none
 integer :: k,i,j,m,n
 overlap_rdm_pos_in_e_pos_eigvectors = 0.d0
 do k = 1, N_states
  do i = 1, mo_num
   do j = 1, mo_num
    do m = 1, ao_num
     do n = 1, ao_num
      overlap_rdm_pos_in_e_pos_eigvectors(j,i,k) += ao_overlap(n,m) * & 
       rdm_pos_in_e_pos_eigvectors_ao(n,j,k) * rdm_pos_in_e_pos_eigvectors_ao(m,i,k)
     enddo
    enddo
   enddo
  enddo
 enddo
 END_PROVIDER 

 BEGIN_PROVIDER [ double precision, overlap_rdm_elec_in_e_pos_eigvectors, (mo_num, mo_num, n_states)]
 implicit none
 integer :: k,i,j,m,n
 overlap_rdm_elec_in_e_pos_eigvectors = 0.d0
 do k = 1, N_states
  do i = 1, mo_num
   do j = 1, mo_num
    do m = 1, ao_num
     do n = 1, ao_num
      overlap_rdm_elec_in_e_pos_eigvectors(j,i,k) += ao_overlap(n,m) * & 
       rdm_elec_in_e_pos_eigvectors_ao(n,j,k) * rdm_elec_in_e_pos_eigvectors_ao(m,i,k)
     enddo
    enddo
   enddo
  enddo
 enddo
 END_PROVIDER 

subroutine pos_natorb_in_r(r, mos_array)
 implicit none
 double precision, intent(in) :: r(3)
 double precision, intent(out) :: mos_array(mo_num)
 double precision :: mos_array_tmp(mo_num)
 integer :: i,k
 call give_all_mos_pos_at_r(r,mos_array_tmp)
 mos_array = 0.d0
 do i = 1, mo_num
  do k = 1, mo_num
   mos_array(i) += rdm_pos_in_e_pos_eigvectors(k,i,1) * mos_array_tmp(k)
  enddo
 enddo
end

subroutine elec_natorb_in_r(r, mos_array)
 implicit none
 double precision, intent(in) :: r(3)
 double precision, intent(out) :: mos_array(mo_num)
 double precision :: mos_array_tmp(mo_num)
 integer :: i,k
 call give_all_mos_at_r(r,mos_array_tmp)
 mos_array = 0.d0
 do i = 1, mo_num
  do k = 1, mo_num
   mos_array(i) += rdm_elec_in_e_pos_eigvectors(k,i,1) * mos_array_tmp(k)
  enddo
 enddo

end
