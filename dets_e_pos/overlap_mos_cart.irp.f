BEGIN_PROVIDER [double precision,overlap_mos_cart_HF,(3,mo_num)]
 implicit none
 integer           :: i,j,ipoint
 double precision  :: nrm,weight,mo,r(3)
 overlap_mos_cart_HF=0.d0
 do ipoint=1,n_points_final_grid
   do i=1,mo_num
     mo=mos_in_r_array(i,ipoint)
     weight=final_weight_at_r_vector(ipoint)
     r(1:3)=final_grid_points(1:3,ipoint)
     do j=1,3
       overlap_mos_cart_HF(j,i)+=r(j)/dsqrt(dot_product(r,r))*mo*weight
     end do
   end do
 end do
 do i=1,mo_num
   nrm=dot_product(overlap_mos_cart_HF(:,i),overlap_mos_cart_HF(:,i))
   overlap_mos_cart_HF(:,i)=overlap_mos_cart_HF(:,i)/dsqrt(nrm)                                                                            
 end do
END_PROVIDER

BEGIN_PROVIDER [double precision,overlap_mos_cart_CI,(3,elec_alpha_num,n_states)]
 implicit none
 integer           :: i,j,k,ipoint
 double precision  :: nrm,weight,mo,r(3)
 overlap_mos_cart_CI=0.d0
 do k=1,n_states
   do ipoint=1,n_points_final_grid
     do i=1,elec_alpha_num
       mo=dot_product(rdm_elec_in_e_pos_eigvectors_sorted(:,i,k),mos_in_r_array(:,ipoint))
       weight=final_weight_at_r_vector(ipoint)
       r(1:3)=final_grid_points(1:3,ipoint)
       do j=1,3
         overlap_mos_cart_CI(j,i,k)+=r(j)/dsqrt(dot_product(r,r))*mo*weight
       end do
     end do
   end do
   do i=1,elec_alpha_num
     nrm=dot_product(overlap_mos_cart_CI(:,i,k),overlap_mos_cart_CI(:,i,k))
     overlap_mos_cart_CI(:,i,k)=overlap_mos_cart_CI(:,i,k)/dsqrt(nrm)
   end do
 end do
END_PROVIDER
