program test_read
 implicit none
 read_wf = .True.
 touch read_wf 
 call routine

end

subroutine routine
 implicit none
 integer :: i,nx,j
 double precision :: r(3), xmax,dx,dens
 double precision, allocatable :: elec_mos_array(:),pos_mos_array(:)
 double precision, allocatable :: pos_nos_array(:),elec_nos_array(:)
 character*(128) :: output
 integer :: i_unit_pos,i_unit_elec,getUnitAndOpen
 output=trim(ezfio_filename)//'.pos_natorb_mos'
 i_unit_pos  = getUnitAndOpen(output,'w')
 output=trim(ezfio_filename)//'.e_natorb_mos'
 i_unit_elec = getUnitAndOpen(output,'w')

 allocate(pos_nos_array(mo_num),elec_nos_array(mo_num))
 allocate(elec_mos_array(mo_num),pos_mos_array(mo_num))
 nx = 50000
 xmax = 50.d0
 dx = xmax/dble(nx)
 r = 0.d0
 do i = 1, nx
  call give_all_mos_pos_at_r(r,pos_mos_array)
  call give_all_mos_at_r(r,elec_mos_array)
  call pos_natorb_in_r(r, pos_nos_array)
  call elec_natorb_in_r(r, elec_nos_array)
  dens = 0.d0
  do j = 1, mo_num
   dens += rdm_pos_in_e_pos_eigvalues(j,1) * pos_nos_array(j)**2
  enddo
  dens = dsqrt(dens)
  write(i_unit_pos,'(100(F16.10,X))')r(1),pos_mos_array(1), pos_nos_array(mo_num),dens
  write(i_unit_elec,'(100(F16.10,X))')r(1),elec_mos_array(1), elec_nos_array(mo_num)
  r(1) += dx
 enddo
 


end
