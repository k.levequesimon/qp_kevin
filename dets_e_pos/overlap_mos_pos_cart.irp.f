BEGIN_PROVIDER [double precision,overlap_mos_pos_cart_CI,(3,n_states)]
 implicit none
 integer           :: j,k,ipoint
 double precision  :: nrm,weight,mo,r(3)
 overlap_mos_pos_cart_CI=0.d0
 do k=1,n_states
   do ipoint=1,n_points_final_grid
       mo=dot_product(rdm_pos_in_e_pos_eigvectors(:,mo_num,k),mos_pos_in_r_array(:,ipoint))
       weight=final_weight_at_r_vector(ipoint)
       r(1:3)=final_grid_points(1:3,ipoint)
       do j=1,3
         overlap_mos_pos_cart_CI(j,k)+=r(j)/dsqrt(dot_product(r,r))*mo*weight
       end do
   end do
   nrm=dot_product(overlap_mos_pos_cart_CI(:,k),overlap_mos_pos_cart_CI(:,k))
   overlap_mos_pos_cart_CI(:,k)=overlap_mos_pos_cart_CI(:,k)/dsqrt(nrm)
 end do
END_PROVIDER
