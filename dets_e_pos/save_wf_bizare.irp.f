program save_chelou
 implicit none
 read_wf = .True.
 touch read_wf
 call save_wf_bla

end

subroutine save_wf_bla
 implicit none
 use bitmasks
 integer :: i,j,n_tot,itmp
 integer(bit_kind), allocatable :: psi_save(:,:,:) 
 double precision, allocatable :: psi_coef_tmp(:,:)
 n_tot = 1 + n_doubles_beta_beta + n_doubles_alpha_alpha + n_singles_beta + n_singles_alpha
 n_tot += n_doubles_alpha_beta
 allocate(psi_save(N_int,2,n_tot))
 allocate(psi_coef_tmp(n_tot,N_states))
 itmp = 1
 psi_save(1:N_int,1:2, itmp) = HF_bitmask(1:N_int,1:2)
 psi_coef_tmp(itmp,1) = 1.d0
 do i = 1, n_doubles_beta_beta
  itmp += 1
  j = list_double_beta_beta(i)
  psi_save(1:N_int,1:2,itmp) = psi_det(1:N_int,1:2,j)
  psi_coef_tmp(itmp,1) = psi_coef(j,1)
 enddo
 do i = 1, n_doubles_alpha_alpha
  itmp += 1
  j = list_double_alpha_alpha(i)
  psi_save(1:N_int,1:2,itmp) = psi_det(1:N_int,1:2,j)
  psi_coef_tmp(itmp,1) = psi_coef(j,1)
 enddo
 do i = 1, n_doubles_alpha_beta
  itmp += 1
  j = list_double_alpha_beta(i)
  psi_save(1:N_int,1:2,itmp) = psi_det(1:N_int,1:2,j)
  psi_coef_tmp(itmp,1) = psi_coef(j,1)
 enddo
 do i = 1, n_singles_alpha
  itmp += 1
  j = list_singles_alpha(i)
  psi_save(1:N_int,1:2,itmp) = psi_det(1:N_int,1:2,j)
  psi_coef_tmp(itmp,1) = psi_coef(j,1)
 enddo
 do i = 1, n_singles_beta
  itmp += 1
  j = list_singles_beta(i)
  psi_save(1:N_int,1:2,itmp) = psi_det(1:N_int,1:2,j)
  psi_coef_tmp(itmp,1) = psi_coef(j,1)
 enddo
 print*,'itmp,n_tot',itmp,n_tot
 call save_wavefunction_general(n_tot,N_states,psi_save,N_states,psi_coef_tmp)

end
