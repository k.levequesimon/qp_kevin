program bla
 call write_mo_pos_new
 call write_mo_new
end

subroutine write_mo_new
 implicit none
 integer :: i,j,i_mo
 double precision, allocatable :: mo_coef_tmp(:,:)
 i_mo = mo_num+1
 allocate(mo_coef_tmp(ao_num+1,mo_num+1))
 mo_coef_tmp = 0.d0
 do i = 1, mo_num
  do j = 1, ao_num
   mo_coef_tmp(j,i) = mo_coef(j,i)
  enddo
 enddo
 mo_coef_tmp(ao_num+1,i_mo) = 1.d0
 ! gram-shmidt
 do i = 1, mo_num+1
  do j = 1, ao_num+1
   write(33,'(3X,F20.16)')mo_coef_tmp(j,i)
  enddo
 enddo
end


subroutine write_mo_pos_new
 implicit none
 integer :: i,j,i_mo
 double precision, allocatable :: mo_coef_pos_tmp(:,:)
 i_mo = mo_num+1
 allocate(mo_coef_pos_tmp(ao_num+1,mo_num+1))
 mo_coef_pos_tmp = 0.d0
 do i = 1, mo_num
  do j = 1, ao_num
   mo_coef_pos_tmp(j,i) = mo_coef_pos(j,i)
  enddo
 enddo
 mo_coef_pos_tmp(ao_num+1,i_mo) = 1.d0
 ! gram-shmidt
 do i = 1, mo_num+1
  do j = 1, ao_num+1
   write(34,'(3X,F20.16)')mo_coef_pos_tmp(j,i)
  enddo
 enddo
end

