program data_cisd_e_pos
  implicit none
  read_wf = .True.
  touch read_wf
  call treatment_from_cisd_e_pos
end program data_cisd_e_pos

subroutine treatment_from_cisd_e_pos
  !select_data = 0: positronic orbitals and effective Coulomb potential for PsCl production cross sections
  !select_data = 1: electronic orbitals for atoms with and without positron
  !select_data = 2: effective Coulomb potential for ionization cross sections 
  implicit none
  integer                        :: i,j,q,k,kvar,kmax
  double precision               :: arg,rdm_ao(2),rdm_mo(2),VE1,VE2,VH,accu,pi,mu,r(3)
  double precision, allocatable  :: tab(:),VH_nstates(:),accu_nstates(:),mos_at_r(:)
  double precision, allocatable  :: erf_kl_ao(:,:)
  character(len=1)               :: kstr
  parameter (pi=3.14159265358979d0)
  parameter (mu=1.d+8)
  if (positron.eqv..true.) then
    open(unit=8,file="eigenvalues_rdm_pos",form="formatted")
    do k=1,n_states
      do j=1,10
        write(8,*) k,mo_num-j+1,rdm_pos_in_e_pos_eigvalues(mo_num-j+1,k) 
      end do
    end do
    close(8)
  end if
  if (n_states.eq.1) kmax=1
  if (n_states.eq.2) kmax=2
  if (n_states.eq.6) kmax=3
  r=0.d0
  if (select_data.eq.0) positron=.true.
  allocate(mos_at_r(mo_num))
  if (select_data.ne.1) then
    do k=1,kmax
      write(kstr,'(i1)') k   
      open(unit=16+k,file="vp_from_cisd_e_pos_N"//trim(kstr),form="formatted")
    end do  
    allocate(erf_kl_ao(ao_num,ao_num),VH_nstates(kmax),accu_nstates(kmax))
  end if
  if (select_data.ne.2) then 
    do k=1,kmax
      write(kstr,'(i1)') k   
      open(unit=10+k,file="wf_from_cisd_e_pos_N"//trim(kstr),form="formatted")
    end do
  end if
  do q=1,npts_grid
    r(3)=dfloat(q)*step_grid
    if (select_data.eq.0) call give_all_mos_pos_at_r(r,mos_at_r)
    if (select_data.ne.0) call give_all_mos_at_r(r,mos_at_r)
    if (select_data.ne.1) call give_all_erf_kl_ao(erf_kl_ao,mu,r)
    if (select_data.eq.0) then    
      allocate(tab(1))
      do k=1,kmax
        if (k.eq.1) call radial_wavefunctions_pos_at_r("CI","1s",mos_at_r,tab(1))
        if (k.eq.2) call radial_wavefunctions_pos_at_r("CI","2p",mos_at_r,tab(1))
        if (k.eq.3) call radial_wavefunctions_pos_at_r("CI","2s",mos_at_r,tab(1))
        tab(:)=tab(:)*r(3)
        write(10+k,'(F13.6,E25.12)') r(3),tab(1)
      end do
      deallocate(tab)
    else if (select_data.eq.1) then
      if (Zc.eq.1) then
        allocate(tab(1))
        do k=1,kmax
          call manage(k,kvar)
          call radial_wavefunctions_at_r("CI",kvar,"1s",mos_at_r,tab(1))
          tab(:)=tab(:)*r(3)
          write(10+k,'(F13.6,E25.12)') r(3),tab(1)
        end do
      else if (Zc.eq.17) then 
        allocate(tab(5))
        do k=1,kmax
          call manage(k,kvar)
          call radial_wavefunctions_at_r("CI",kvar,"1s",mos_at_r,tab(1))
          call radial_wavefunctions_at_r("CI",kvar,"2s",mos_at_r,tab(2))
          call radial_wavefunctions_at_r("CI",kvar,"2p",mos_at_r,tab(3))
          call radial_wavefunctions_at_r("CI",kvar,"3s",mos_at_r,tab(4))
          call radial_wavefunctions_at_r("CI",kvar,"3p",mos_at_r,tab(5))
          tab(:)=tab(:)*r(3)
          write(10+k,'(F13.6,5E25.12)') r(3),(tab(i),i=1,5)
        end do
      end if
      deallocate(tab)
    end if
    if (select_data.ne.1) then
      do k=1,kmax
        VH_nstates(k)=0.d0
        VH=VH_nstates(k)
        call manage(k,kvar)
        !$OMP PARALLEL              &
        !$OMP PRIVATE (i,j,rdm_ao)  & 
        !$OMP SHARED (positron,ao_num,rdm_elec_in_e_pos_full_ao,rdm_pos_in_e_pos_ao,rdm_elec_full_ao)
        !$OMP DO REDUCTION (+:VH) SCHEDULE (guided) 
        !COLLAPSE(2)
        do i=1,ao_num
          do j=1,ao_num
            rdm_ao(:)=0.d0
            if (positron.eqv..true.) then
              rdm_ao(1)=rdm_elec_in_e_pos_full_ao(i,j,kvar)
              if (select_data.eq.2) rdm_ao(2)=-rdm_pos_in_e_pos_ao(i,j,kvar)
            else if (positron.eqv..false.) then
              rdm_ao(1)=rdm_elec_full_ao(i,j,kvar)
            end if
            VH+=sum(rdm_ao)*erf_kl_ao(i,j)   
          end do
        end do
        !$OMP END DO
        !$OMP END PARALLEL
        VH_nstates(k)=VH
      end do
    end if
    if (select_data.eq.0) then
      allocate(tab(2))
      do k=1,kmax
        tab(1)=-dfloat(Zc+1)/r(3)
        tab(2)=VH_nstates(k)
        write(16+k,'(F13.6,E25.12)') r(3),sum(tab)
      end do
      deallocate(tab)
    else if (select_data.eq.2) then 
      do k=1,kmax
        accu_nstates(k)=0.d0
        accu=accu_nstates(k)
        call manage(k,kvar)
        !$OMP PARALLEL                       &
        !$OMP PRIVATE (i,j,rdm_mo,mos_at_r)  & 
        !$OMP SHARED (positron,mo_num,rdm_elec_in_e_pos_full_mo,rdm_pos_in_e_pos_mo,rdm_elec_full_mo)
        !$OMP DO REDUCTION (+:accu) SCHEDULE (guided) 
        !COLLAPSE(2)
        do j=1,mo_num
          do i=1,mo_num
            rdm_mo(:)=0.d0
            if (positron.eqv..true.) then
              rdm_mo(1)=rdm_elec_in_e_pos_full_mo(i,j,kvar)
              rdm_mo(2)=-rdm_pos_in_e_pos_mo(i,j,kvar)
            else if (positron.eqv..false.) then
              rdm_mo(1)=rdm_elec_full_mo(i,j,kvar)
            end if
            accu+=sum(rdm_mo)*mos_at_r(i)*mos_at_r(j)
          end do
        end do
        !$OMP END DO
        !$OMP END PARALLEL
        accu_nstates(k)=accu
      end do
      allocate(tab(4))
      do k=1,kmax
        tab(1)=-dfloat(Zc)/r(3)
        tab(2)=VH_nstates(k)
        tab(3)=-(3.d0/pi*accu_nstates(k))**(1.d0/3.d0) 
        arg=1.d0+11.4d0*(4.d0*pi/3.d0*accu_nstates(k))**(1.d0/3.d0)
        tab(4)=-0.0333d0*dlog(arg)
        VE1=+tab(1)+tab(2)+tab(3)+tab(4)
        VE2=-tab(1)-tab(2)+tab(3)+tab(4)
        write(16+k,'(F13.6,2E25.12)') r(3),VE1,VE2
      end do
      deallocate(tab)
    end if
  end do
  deallocate(mos_at_r)
  if (select_data.ne.1) then
    do k=1,kmax
      close(16+k)
    end do
    deallocate(erf_kl_ao,VH_nstates,accu_nstates)
  end if  
  if (select_data.ne.2) then 
    do k=1,kmax
     close(10+k)
    end do 
  end if
end subroutine treatment_from_cisd_e_pos

subroutine manage(k,kvar)
  implicit none
  integer, intent(in)  :: k
  integer, intent(out) :: kvar
  if (k.eq.1) kvar=1
  if (k.eq.2) kvar=2
  if (k.eq.3) kvar=6
end subroutine manage 
