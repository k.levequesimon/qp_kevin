program data_damp_scf_pos
  implicit none
  read_wf = .True.
  touch read_wf 
  call treatment_from_damp_scf_pos
end program data_damp_scf_pos

subroutine treatment_from_damp_scf_pos
  !select_data = 0: positronic orbitals and effective Coulomb potential for PsCl production cross sections
  !select_data = 1: electronic orbitals for atoms with and without positron
  !select_data = 2: effective Coulomb potential for ionization cross sections 
  implicit none
  integer                        :: i,j,q
  double precision               :: arg,rdm_ao(2),rdm_mo(2),VE1,VE2,VH,accu,pi,mu,r(3)
  double precision, allocatable  :: tab(:),mos_at_r(:),erf_kl_ao(:,:)
  parameter (pi=3.14159265358979d0)
  parameter (mu=1.d+8)
  r=0.d0
  if (select_data.eq.0) positron=.true.
  allocate(mos_at_r(mo_num))
  if (select_data.ne.1) then
    open(unit=11,file="vp_from_damp_scf_pos",form="formatted")
    allocate(erf_kl_ao(ao_num,ao_num))
  end if
  if (select_data.ne.2) open(unit=10,file="wf_from_damp_scf_pos",form="formatted")
  do q=1,npts_grid
    r(3)=dfloat(q)*step_grid
    if (select_data.eq.0) call give_all_mos_pos_at_r(r,mos_at_r)
    if (select_data.ne.0) call give_all_mos_at_r(r,mos_at_r)
    if (select_data.ne.1) call give_all_erf_kl_ao(erf_kl_ao,mu,r)
    if (select_data.eq.0) then 
      allocate(tab(1))
      call radial_wavefunctions_pos_at_r("HF","1s",mos_at_r,tab(1))
      tab(:)=tab(:)*r(3)
      write(10,100) r(3),tab(1)
      100 format (F13.6,E25.12)
      deallocate(tab)
    else if (select_data.eq.1) then
      if (Zc.eq.1) then
        allocate(tab(1))
        call radial_wavefunctions_at_r("HF",1,"1s",mos_at_r,tab(1))
        tab(:)=tab(:)*r(3)
        write(10,104) r(3),tab(1)
        104 format (F13.6,E25.12)
      else if (Zc.eq.17) then
        allocate(tab(5))
        call radial_wavefunctions_at_r("HF",1,"1s",mos_at_r,tab(1))
        call radial_wavefunctions_at_r("HF",1,"2s",mos_at_r,tab(2))
        call radial_wavefunctions_at_r("HF",1,"2p",mos_at_r,tab(3))
        call radial_wavefunctions_at_r("HF",1,"3s",mos_at_r,tab(4))
        call radial_wavefunctions_at_r("HF",1,"3p",mos_at_r,tab(5))
        tab(:)=tab(:)*r(3)
        write(10,101) r(3),(tab(i),i=1,5)
        101 format (F13.6,5E25.12)
      end if
      deallocate(tab)
    end if
    if (select_data.ne.1) then
      VH=0.d0
      !$OMP PARALLEL                &
      !$OMP PRIVATE (i,j,rdm_ao)    & 
      !$OMP SHARED (positron,ao_num,rdm_elec_in_e_pos_full_ao,rdm_pos_in_e_pos_ao,rdm_elec_full_ao)
      !$OMP DO REDUCTION (+:VH) SCHEDULE (guided) 
      !COLLAPSE(2)
      do j=1,ao_num
        do i=1,ao_num
          rdm_ao(:)=0.d0
          if (positron.eqv..true.) then
            rdm_ao(1)=rdm_elec_in_e_pos_full_ao(i,j,1)
            if (select_data.eq.2) rdm_ao(2)=-rdm_pos_in_e_pos_ao(i,j,1)
          else if (positron.eqv..false.) then
            rdm_ao(1)=rdm_elec_full_ao(i,j,1)
          end if
          VH+=sum(rdm_ao)*erf_kl_ao(i,j)
        end do
      end do
      !$OMP END DO
      !$OMP END PARALLEL
    end if  
    if (select_data.eq.0) then
      allocate(tab(2))
      tab(1)=-dfloat(Zc+1)/r(3)
      tab(2)=VH
      write(11,102) r(3),sum(tab)
      102 format (F13.6,E25.12)
      deallocate(tab)
    else if (select_data.eq.2) then  
      accu=0.d0
      !$OMP PARALLEL                &
      !$OMP PRIVATE (i,j,rdm_mo)    & 
      !$OMP SHARED (positron,mo_num,rdm_elec_in_e_pos_full_mo,rdm_pos_in_e_pos_mo,rdm_elec_full_mo)
      !$OMP DO REDUCTION (+:accu) SCHEDULE (guided) 
      !COLLAPSE(2)
      do j=1,mo_num
        do i=1,mo_num
          rdm_mo(:)=0.d0
          if (positron.eqv..true.) then
            rdm_mo(1)=rdm_elec_in_e_pos_full_mo(i,j,1)
            rdm_mo(2)=-rdm_pos_in_e_pos_mo(i,j,1)
          else if (positron.eqv..false.) then
            rdm_mo(1)=rdm_elec_full_mo(i,j,1)
          end if
          accu+=sum(rdm_mo)*mos_at_r(i)*mos_at_r(j)
        end do
      end do
      !$OMP END DO
      !$OMP END PARALLEL
      allocate(tab(4))
      tab(1)=-dfloat(Zc)/r(3)
      tab(2)=VH
      tab(3)=-(3.d0/pi*accu)**(1.d0/3.d0) 
      arg=1.d0+11.4d0*(4.d0*pi/3.d0*accu)**(1.d0/3.d0)
      tab(4)=-0.0333d0*dlog(arg)
      VE1=+tab(1)+tab(2)+tab(3)+tab(4)
      VE2=-tab(1)-tab(2)+tab(3)+tab(4)
      deallocate(tab)
      write(11,103) r(3),VE1,VE2
      103 format (F13.6,2E25.12)
    end if  
  end do
  deallocate(mos_at_r)
  if (select_data.ne.1) then
    close(11)
    deallocate(erf_kl_ao)
  end if  
  if (select_data.ne.2) close(10)
end subroutine treatment_from_damp_scf_pos
