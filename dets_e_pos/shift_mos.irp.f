program pouet
 implicit none
! call shift_mos
 call shift_mos_pos

end

subroutine shift_mos
 implicit none
 integer :: i,j,m,n,i1
 print*, 'MO to shift ?'
 read(*,*)i1
 double precision, allocatable :: mo_coef_tmp(:,:)
 allocate(mo_coef_tmp(ao_num, mo_num))
 mo_coef_tmp = mo_coef
 do m = 1, ao_num
  mo_coef(m,mo_num) = mo_coef(m,i1)
 enddo
 do i = i1, mo_num-1
  do m = 1, ao_num
   mo_coef(m,i) = mo_coef_tmp(m,i+1)
  enddo
 enddo
  call save_mos
end

subroutine shift_mos_pos
 implicit none
 integer :: i,j,m,n,i1
 print*, 'MO to shift ?'
 read(*,*)i1
 double precision, allocatable :: mo_coef_pos_tmp(:,:)
 allocate(mo_coef_pos_tmp(ao_num, mo_num))
 mo_coef_pos_tmp = mo_coef_pos
 do m = 1, ao_num
  mo_coef_pos(m,mo_num) = mo_coef_pos(m,i1)
 enddo
 do i = i1, mo_num-1
  do m = 1, ao_num
   mo_coef_pos(m,i) = mo_coef_pos_tmp(m,i+1)
  enddo
 enddo
  call save_mos_pos
end
