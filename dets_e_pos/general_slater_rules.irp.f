subroutine i_o1_j_pos(key_i, key_j, i_pos, j_pos, ints_e_ij, ints_pos_ij, Nint,o_ij)
 use bitmasks
 BEGIN_DOC
! Matrix element for a one-particle operator between two determinants/positron occupation
!
! the integrals of the corresponding one-particle operator are ints_e_ij for the electrons 
!
! and ints_pos_ij for the positron 
!
! it enventually returns o_ij
 END_DOC
 implicit none
 integer, intent(in)            :: Nint, i_pos, j_pos
 integer(bit_kind), intent(in)  :: key_i(Nint,2), key_j(Nint,2)
 double precision, intent(in)   :: ints_e_ij(mo_num, mo_num), ints_pos_ij(mo_num, mo_num)
 double precision, intent(out)  :: o_ij

 integer                        :: exc(0:2,2,2)
 integer                        :: degree
 double precision               :: get_two_e_integral
 integer                        :: h,n,p,q
 integer                        :: i,j,k
 integer                        :: occ(Nint*bit_kind_size,2)
 double precision               :: diag_H_mat_elem, phase
 double precision               :: pos_single_exc_H_mat_elem,pos_diag_H_mat_elem
 integer                        :: n_occ_ab(2), degree_pos, degree_tot

 ASSERT (Nint > 0)
 ASSERT (Nint == N_int)
 ASSERT (sum(popcnt(key_i(:,1))) == elec_alpha_num)
 ASSERT (sum(popcnt(key_i(:,2))) == elec_beta_num)
 ASSERT (sum(popcnt(key_j(:,1))) == elec_alpha_num)
 ASSERT (sum(popcnt(key_j(:,2))) == elec_beta_num)


 o_ij = 0.d0
 !DIR$ FORCEINLINE
 call get_excitation_degree(key_i,key_j,degree,Nint)
 integer :: spin
 ! excitation degree of the positron
 degree_pos = 0
 if(i_pos .ne. j_pos)then 
  degree_pos += 1
 endif

 ! total excitation degree
 degree_tot = degree_pos + degree
 if(degree_tot.gt.1)then
  return
 endif

 if(degree_tot == 1)then 
    if (degree == 1)then 
       call get_single_excitation(key_i,key_j,exc,phase,Nint)
       !DIR$ FORCEINLINE
       call bitstring_to_list_ab(key_i, occ, n_occ_ab, Nint)
       ! find the holes and particles from key_i --> key_j 
       if (exc(0,1,1) == 1) then
         ! Single alpha
         h = exc(1,1,1) ! hole
         p = exc(1,2,1) ! particle
         spin = 1
       else
         ! Single beta
         h = exc(1,1,2) ! hole
         p = exc(1,2,2) ! particle
         spin = 2
       endif
       o_ij += phase * ints_e_ij(h,p)
    else if(degree_pos == 1)then
     o_ij += ints_pos_ij(i_pos,j_pos)
    endif
 else if(degree_tot == 0)then
   call bitstring_to_list_ab(key_i, occ, n_occ_ab, Nint)
   o_ij = 0.d0
   integer :: ispin
   do ispin = 1, 2
    do i = 1, n_occ_ab(ispin)
     h = occ(i,ispin)
     o_ij += ints_e_ij(h,h)
    enddo
   enddo
   o_ij += ints_pos_ij(i_pos,i_pos)
 endif

end

subroutine i_o1_j(key_i, key_j, ints_e_ij, Nint, o_ij)
 use bitmasks
 BEGIN_DOC
! Matrix element for a one-particle operator between two determinants 
!
! the integrals of the corresponding one-particle operator are ints_e_ij 
!
! it enventually returns o_ij
 END_DOC
 implicit none
 integer, intent(in)            :: Nint
 integer(bit_kind), intent(in)  :: key_i(Nint,2), key_j(Nint,2)
 double precision, intent(in)   :: ints_e_ij(mo_num, mo_num)
 double precision, intent(out)  :: o_ij

 integer                        :: exc(0:2,2,2)
 integer                        :: degree
 double precision               :: get_two_e_integral
 integer                        :: h,n,p,q
 integer                        :: i,j,k
 integer                        :: occ(Nint*bit_kind_size,2)
 double precision               :: diag_H_mat_elem, phase
 integer                        :: n_occ_ab(2), degree_tot

 ASSERT (Nint > 0)
 ASSERT (Nint == N_int)
 ASSERT (sum(popcnt(key_i(:,1))) == elec_alpha_num)
 ASSERT (sum(popcnt(key_i(:,2))) == elec_beta_num)
 ASSERT (sum(popcnt(key_j(:,1))) == elec_alpha_num)
 ASSERT (sum(popcnt(key_j(:,2))) == elec_beta_num)


 o_ij = 0.d0
 !DIR$ FORCEINLINE
 call get_excitation_degree(key_i,key_j,degree,Nint)
 integer :: spin
 ! total excitation degree
 degree_tot = degree
 if(degree_tot.gt.1)then
  return
 endif

 
   select case (degree)
     case (1)
        call get_single_excitation(key_i,key_j,exc,phase,Nint)
        !DIR$ FORCEINLINE
        call bitstring_to_list_ab(key_i, occ, n_occ_ab, Nint)
        ! find the holes and particles from key_i --> key_j 
        if (exc(0,1,1) == 1) then
          ! Single alpha
          h = exc(1,1,1) ! hole
          p = exc(1,2,1) ! particle
          spin = 1
        else
          ! Single beta
          h = exc(1,1,2) ! hole
          p = exc(1,2,2) ! particle
          spin = 2
        endif
        
        o_ij += phase * ints_e_ij(h,p)
     case (0)
       call bitstring_to_list_ab(key_i, occ, n_occ_ab, Nint)
       o_ij = 0.d0
       integer :: ispin
       do ispin = 1, 2
        do i = 1, n_occ_ab(ispin)
         h = occ(i,ispin)
         o_ij += ints_e_ij(h,h)
        enddo
       enddo
   end select

end


subroutine i_o1_o2_j(key_i, key_j, i_pos, j_pos, ints_e_ij, ints_pos_ij, ints_two_e, ints_e_pos, Nint,o1_ij, o2_ij, o2_ep)
 use bitmasks
 BEGIN_DOC
 Matrix element for a one-particle and two-particle operator between two determinants/positron occupation

 the integrals of the corresponding one-particle operator are ints_e_ij for the electrons 

 and ints_pos_ij for the positron 

 ints_two_e(i,j,k,l) = <ij | o2 | kl>

 ints_e_pos(i,j,k,l) = <i_e j_pos | o2 | k_e l_pos>

 it enventually returns o_ij
 END_DOC
 implicit none
 integer, intent(in)            :: Nint, i_pos, j_pos
 integer(bit_kind), intent(in)  :: key_i(Nint,2), key_j(Nint,2)
 double precision, intent(in)   :: ints_e_ij(mo_num, mo_num), ints_pos_ij(mo_num, mo_num)
 double precision, intent(in)   :: ints_two_e(mo_num, mo_num, mo_num, mo_num)
 double precision, intent(in)   :: ints_e_pos(mo_num, mo_num, mo_num, mo_num)
 double precision, intent(out)  :: o1_ij, o2_ij, o2_ep

 integer                        :: exc(0:2,2,2)
 integer                        :: degree
 double precision               :: get_two_e_integral
 integer                        :: h,n,p,q
 integer                        :: i,j,k
 integer                        :: occ(Nint*bit_kind_size,2)
 double precision               :: diag_H_mat_elem, phase
 double precision               :: pos_single_exc_H_mat_elem,pos_diag_H_mat_elem
 integer                        :: n_occ_ab(2), degree_pos, degree_tot

 integer :: spin,other_spin(2)
 other_spin(1) = 2
 other_spin(2) = 1
!
 o1_ij = 0.d0
 o2_ij = 0.d0
 o2_ep = 0.d0
 !DIR$ FORCEINLINE
 call get_excitation_degree(key_i,key_j,degree,Nint)
 ! excitation degree of the positron
 degree_pos = 0
 if(i_pos .ne. j_pos)then 
  degree_pos += 1
 endif

 ! total excitation degree
 degree_tot = degree_pos + degree
 if(degree_tot.gt.2)then
  return
 endif

! print*,'degree = ',degree,degree_pos,degree_tot
! print*,'i_pos,j_pos',i_pos,j_pos
 
 if(degree_tot == 0 )then !!! DIAGONAL MATRIX ELEMENT 
  call bitstring_to_list_ab(key_i, occ, n_occ_ab, Nint)
  o1_ij = 0.d0
  o1_ij += ints_pos_ij(i_pos,i_pos) ! positron one-body
  do ispin = 1, 2 ! one-e term from all spins 
   do i = 1, n_occ_ab(ispin)
    h = occ(i,ispin)
    o1_ij += ints_e_ij(h,h)
   enddo
  enddo
  integer :: ispin , jspin
  ! two-e term from the same spin 
  o2_ij = 0.d0
  do ispin = 1, 2
   do i = 1, n_occ_ab(ispin)
    h = occ(i,ispin)
    do j = i+1, n_occ_ab(ispin)
     p = occ(j,ispin)
     o2_ij += ints_two_e(h,p,h,p) - ints_two_e(h,p,p,h)
    enddo
   enddo
  enddo
  ! two-e term from different spins 
  ispin = 1
  jspin = 2
  do i = 1, n_occ_ab(ispin)
   h = occ(i,ispin)
   do j = 1, n_occ_ab(jspin)
    p = occ(j,jspin)
    o2_ij += ints_two_e(h,p,h,p) 
   enddo
  enddo
  ! e-pos terms 
  do ispin = 1, 2
   do i = 1, n_occ_ab(ispin)
    h = occ(i,ispin)
    o2_ij += ints_e_pos(h,i_pos,h,i_pos)
    o2_ep += ints_e_pos(h,i_pos,h,i_pos)
   enddo
  enddo
 else if (degree_tot == 2 .and. degree == 2)then  !! PURE DOUBLE EXCITATION FROM ELECTRONS 
   call get_double_excitation(key_i,key_j,exc,phase,Nint)
   if (exc(0,1,1) == 1) then
     ! Single alpha, single beta
       o2_ij = phase*ints_two_e(exc(1,1,1),exc(1,1,2),exc(1,2,1),exc(1,2,2))
   else if (exc(0,1,1) == 2) then
     ! Double alpha
     o2_ij = phase*(ints_two_e(exc(1,1,1),exc(2,1,1),exc(1,2,1),exc(2,2,1))  &
                   -ints_two_e(exc(1,1,1),exc(2,1,1),exc(2,2,1),exc(1,2,1)) )
         
   else if (exc(0,1,2) == 2) then
     ! Double beta
     o2_ij = phase*(ints_two_e(exc(1,1,2),exc(2,1,2),exc(1,2,2),exc(2,2,2))  &
                   -ints_two_e(exc(1,1,2),exc(2,1,2),exc(2,2,2),exc(1,2,2)) )
   endif
 else if(degree_tot == 1 .and. degree == 1)then !! PURE SINGLE FROM ELECTRONS 
   ! single excitation of electrons 
   call get_single_excitation(key_i,key_j,exc,phase,Nint)
   !DIR$ FORCEINLINE
   call bitstring_to_list_ab(key_i, occ, n_occ_ab, Nint)
   ! find the holes and particles from key_i --> key_j 
   if (exc(0,1,1) == 1) then
     ! Single alpha
     h = exc(1,1,1) ! hole
     p = exc(1,2,1) ! particle
     spin = 1
   else
     ! Single beta
     h = exc(1,1,2) ! hole
     p = exc(1,2,2) ! particle
     spin = 2
   endif
   o1_ij = phase * ints_e_ij(h,p) ! direct one body term 
   o2_ij = 0.d0 ! two-e terms 
   do i = 1, n_occ_ab(spin) ! from the same spin 
    j = occ(i,spin)
    o2_ij += phase * (ints_two_e(j,h,j,p) - ints_two_e(j,h,p,j))
   enddo
   do i = 1, n_occ_ab(other_spin(spin)) ! from the other spin 
    j = occ(i,other_spin(spin))
    o2_ij += phase * ints_two_e(j,h,j,p) 
   enddo
   ! extra contribution from the e-pos interaction 
   o2_ij += phase * ints_e_pos(h,i_pos,p,i_pos) ! term coming from the positron 
   o2_ep += phase * ints_e_pos(h,i_pos,p,i_pos) ! term coming from the positron 
 else if(degree_pos == 1 .and. degree == 0)then !!! PURE SINGLE FROM POSITRON 
    o1_ij += ints_pos_ij(i_pos,j_pos)
    call bitstring_to_list_ab(key_i, occ, n_occ_ab, Nint)
    do ispin = 1, 2 
     do i = 1, n_occ_ab(ispin) ! n_ab(ispin)  == number of electron of spin 'ispin'
      h = occ(i,ispin) ! index of the orbital occupied by the ith electron  of spin ispin
      o2_ij += ints_e_pos(h,i_pos,h,j_pos)
      o2_ep += ints_e_pos(h,i_pos,h,j_pos)
     enddo
    enddo
 else  if(degree_pos == 1 .and.  degree == 1)then ! single excitation electron // single excitation positron 
     call get_single_excitation(key_i,key_j,exc,phase,Nint)
     !DIR$ FORCEINLINE
     call bitstring_to_list_ab(key_i, occ, n_occ_ab, Nint)
     if (exc(0,1,1) == 1) then
       ! Single alpha
       h = exc(1,1,1) ! hole
       p = exc(1,2,1) ! particle
       spin = 1
     else
       ! Single beta
       h = exc(1,1,2) ! hole
       p = exc(1,2,2) ! particle
       spin = 2
     endif
     o2_ij +=  phase * ints_e_pos(h,i_pos,p,j_pos)  
     o2_ep +=  phase * ints_e_pos(h,i_pos,p,j_pos) 
 endif
!
end

