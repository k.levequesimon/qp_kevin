 use bitmasks

BEGIN_PROVIDER [ integer(bit_kind), det_exc_ref, (N_int,2)]
 implicit none
 integer :: h1,p1,s1,i_ok
 h1 = i_orb_exc
 p1 = elec_alpha_num
 s1 = 2 
 det_exc_ref = HF_bitmask
 call do_single_excitation(det_exc_ref,h1,p1,s1,i_ok)
 if(i_ok == -1)then
   print*,'excitation was not possible '
   print*,'det_exc_ref was a problem'
   stop
 endif
END_PROVIDER 

BEGIN_PROVIDER [ integer, index_exc_det]
 implicit none
 BEGIN_DOC
 ! index of the determinant in the excited that is of interest
 END_DOC
 integer :: l,degree
 do l = 1, n_det_total_e_pos
  call get_excitation_degree(det_exc_ref,psi_total_e_pos(1,1,l),degree,N_int)
  if(degree == 0 .and. occ_total_e_pos(l) == 1)then
    index_exc_det = l
    exit
  endif
 enddo
END_PROVIDER 

BEGIN_PROVIDER [ double precision, coef_det_exc_ref, (N_states)]
 implicit none
 integer :: i,j
 do i = 1, N_states
  coef_det_exc_ref(i) = cisd_e_pos_coef(index_exc_det, i)
 enddo
END_PROVIDER 

 BEGIN_PROVIDER [ integer, sorted_istate, (N_states) ]
&BEGIN_PROVIDER [ double precision, sorted_coef, (N_states) ]
 implicit none
 double precision :: array(N_states)
 integer :: iorder(N_states),i,j 
 do i = 1, N_states
  array(i) = -dabs(coef_det_exc_ref(i))
  iorder(i) = i
 enddo
 call dsort(array,iorder,N_states)
 sorted_istate = iorder
 do i = 1, N_states
  sorted_coef(i)   = dabs(array(i))
 enddo
END_PROVIDER 


BEGIN_PROVIDER [ double precision, energy_sorted_exc, (N_states)]
 implicit none
 integer :: i
 do i = 1, N_states
  energy_sorted_exc(i)= cisd_e_pos_energies(sorted_istate(i))
 enddo
END_PROVIDER 
