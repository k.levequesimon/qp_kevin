
BEGIN_PROVIDER [ double precision, SCF_energy_pos ]
 implicit none
 BEGIN_DOC
 ! Energy for the system with electrons and positrons, it is computed with Fock matrices. 
 END_DOC
 SCF_energy_pos = nuclear_repulsion

 integer                        :: i,j
 do j=1,ao_num
   do i=1,ao_num
     SCF_energy_pos += 0.5d0 * (                                          &
         (ao_one_e_integrals(i,j) + Fock_matrix_ao_w_pos_alpha(i,j) ) *  SCF_density_matrix_ao_alpha(i,j) +&
         (ao_one_e_integrals(i,j) + Fock_matrix_ao_w_pos_beta(i,j) ) *  SCF_density_matrix_ao_beta (i,j) + & 
          (ao_pos_integrals(i,j)   + Fock_matrix_ao_pos(i,j) )  *  SCF_density_matrix_pos(i,j) ) 
   enddo
 enddo
!SCF_energy_pos += extra_e_contrib_density

END_PROVIDER



BEGIN_PROVIDER [ double precision, ao_pos_integrals, (ao_num, ao_num) ]
 implicit none
 BEGIN_DOC
! one body integrals for positrons on the AO basis: kinetic "minus" coulomb
!
! ao_pos_integrals(i,j) = <X_i| T - \sum_{k=1}^{nucl_num} Z_k/(r - r_k) |X_j>
 END_DOC
 ao_pos_integrals = ao_kinetic_integrals - ao_integrals_n_e
END_PROVIDER 

BEGIN_PROVIDER [ double precision, mo_pos_integrals, (mo_num, mo_num) ]
 implicit none
 BEGIN_DOC
! one body integrals for positrons on the MO basis: kinetic "minus" coulomb
!
! ao_pos_integrals(i,j) = <phi_i| T - \sum_{k=1}^{nucl_num} Z_k/(r - r_k) |phi_j>
 END_DOC
 ao_pos_integrals = ao_kinetic_integrals - ao_integrals_n_e
    call ao_to_mo_pos(                                            &
        ao_pos_integrals,                                         &
        size(ao_pos_integrals,1),                                 &
        mo_pos_integrals,                                         &
        size(mo_pos_integrals,1)                                  &
        )
END_PROVIDER 



BEGIN_PROVIDER [double precision, SCF_density_matrix_pos, (ao_num,ao_num) ]
   implicit none
   BEGIN_DOC
   ! $C.C^t$ over the positrons MOs
   !
   ! This the density of the positrons on the AO basis 
   END_DOC
!   call dgemm('N','T',ao_num,ao_num,n_positrons,1.d0, &
!        mo_coef_pos, size(mo_coef_pos,1), &
!        mo_coef_pos, size(mo_coef_pos,1), 0.d0, &
!        SCF_density_matrix_pos, size(SCF_density_matrix_pos,1))
  SCF_density_matrix_pos = 0.d0
  integer :: i,j,k

 do k = 1, n_positrons
  do i = 1, ao_num
   do j = 1, ao_num
    SCF_density_matrix_pos(j,i) += mo_coef_pos(j,k) * mo_coef_pos(i,k)
   enddo
  enddo
 enddo
END_PROVIDER

 BEGIN_PROVIDER [ double precision, ao_two_e_integral_alpha_w_pos, (ao_num, ao_num) ]
&BEGIN_PROVIDER [ double precision, ao_two_e_integral_beta_w_pos , (ao_num, ao_num) ]
&BEGIN_PROVIDER [ double precision, ao_two_e_integral_pos      , (ao_num, ao_num) ]
 use map_module
 implicit none
 BEGIN_DOC
 ! ao_two_e_integral_alpha_w_pos(i,j) = two-body contribution to the elec ALPHA Fock matrices in AO basis including e-positron interaction
 !
 ! ao_two_e_integral_beta_w_pos(i,j) = two-body contribution to the elec BETA Fock matrices in AO basis including e-positron interaction
 !
 ! ao_two_e_integral_pos(i,j) = two-body contribution to the positron Fock matrix in AO basis including e-positron interaction
 ! 
 END_DOC

 integer                        :: i,j,k,l,k1,r,s
 integer                        :: i0,j0,k0,l0
 integer*8                      :: p,q
 double precision               :: integral, c0, c1, c2
 double precision               :: ao_two_e_integral, local_threshold
 double precision, allocatable  :: ao_two_e_integral_alpha_w_pos_tmp(:,:)
 double precision, allocatable  :: ao_two_e_integral_beta_w_pos_tmp(:,:)
 double precision, allocatable  :: ao_two_e_integral_pos_tmp(:,:)

 ao_two_e_integral_alpha_w_pos = 0.d0
 ao_two_e_integral_beta_w_pos  = 0.d0
 ao_two_e_integral_pos  = 0.d0
   PROVIDE ao_two_e_integrals_in_map

   integer(omp_lock_kind) :: lck(ao_num)
   integer(map_size_kind)     :: i8
   integer                        :: ii(8), jj(8), kk(8), ll(8), k2
   integer(cache_map_size_kind)   :: n_elements_max, n_elements
   integer(key_kind), allocatable :: keys(:)
   double precision, allocatable  :: values(:)

   !$OMP PARALLEL DEFAULT(NONE)                                      &
       !$OMP PRIVATE(i,j,l,k1,k,integral,ii,jj,kk,ll,i8,keys,values,n_elements_max, &
       !$OMP  n_elements,ao_two_e_integral_alpha_w_pos_tmp,ao_two_e_integral_beta_w_pos_tmp,ao_two_e_integral_pos_tmp)&
       !$OMP SHARED(ao_num,SCF_density_matrix_ao_alpha,SCF_density_matrix_ao_beta,SCF_density_matrix_pos,&
       !$OMP  ao_integrals_map, ao_two_e_integral_alpha_w_pos, ao_two_e_integral_beta_w_pos, ao_two_e_integral_pos)

   call get_cache_map_n_elements_max(ao_integrals_map,n_elements_max)
   allocate(keys(n_elements_max), values(n_elements_max))
   allocate(ao_two_e_integral_alpha_w_pos_tmp(ao_num,ao_num), &
            ao_two_e_integral_beta_w_pos_tmp(ao_num,ao_num),  &
            ao_two_e_integral_pos_tmp(ao_num,ao_num))
   ao_two_e_integral_alpha_w_pos_tmp = 0.d0
   ao_two_e_integral_beta_w_pos_tmp  = 0.d0
   ao_two_e_integral_pos_tmp  = 0.d0

   !$OMP DO SCHEDULE(static,1)
   do i8=0_8,ao_integrals_map%map_size
     n_elements = n_elements_max
     call get_cache_map(ao_integrals_map,i8,keys,values,n_elements)
     do k1=1,n_elements
       call two_e_integrals_index_reverse(kk,ii,ll,jj,keys(k1))

       do k2=1,8
         if (kk(k2)==0) then
           cycle
         endif
         i = ii(k2)
         j = jj(k2)
         k = kk(k2)
         l = ll(k2)
         integral = (SCF_density_matrix_ao_alpha(k,l)+SCF_density_matrix_ao_beta(k,l)-SCF_density_matrix_pos(k,l)) * values(k1)
         ao_two_e_integral_alpha_w_pos_tmp(i,j) += integral
         ao_two_e_integral_beta_w_pos_tmp (i,j) += integral
         ao_two_e_integral_pos_tmp (i,j) -= (SCF_density_matrix_ao_alpha(k,l)+SCF_density_matrix_ao_beta(k,l)) * values(k1) 
         integral = values(k1)
         ao_two_e_integral_alpha_w_pos_tmp(l,j) -= SCF_density_matrix_ao_alpha(k,i) * integral
         ao_two_e_integral_beta_w_pos_tmp (l,j) -= SCF_density_matrix_ao_beta(k,i) * integral
       enddo
     enddo
   enddo
   !$OMP END DO NOWAIT
   !$OMP CRITICAL
   ao_two_e_integral_alpha_w_pos += ao_two_e_integral_alpha_w_pos_tmp
   ao_two_e_integral_beta_w_pos  += ao_two_e_integral_beta_w_pos_tmp
   ao_two_e_integral_pos  += ao_two_e_integral_pos_tmp
   !$OMP END CRITICAL
   deallocate(keys,values,ao_two_e_integral_alpha_w_pos_tmp,ao_two_e_integral_beta_w_pos_tmp,ao_two_e_integral_pos_tmp)
   !$OMP END PARALLEL


END_PROVIDER

BEGIN_PROVIDER [ double precision, Fock_matrix_ao_pos,  (ao_num, ao_num) ]
 implicit none
 BEGIN_DOC
! Fock matrix for the positrons on the AO basis
 END_DOC

 integer                        :: i,j
 do j=1,ao_num
   do i=1,ao_num
     Fock_matrix_ao_pos(i,j) = ao_pos_integrals(i,j) + ao_two_e_integral_pos(i,j) 
   enddo
 enddo

END_PROVIDER 

BEGIN_PROVIDER [ double precision, Fock_matrix_mo_pos, (mo_num,mo_num) ]
   implicit none
   BEGIN_DOC
   ! Fock matrix on the MO basis
   END_DOC
   call ao_to_mo_pos(Fock_matrix_ao_pos,size(Fock_matrix_ao_pos,1), &
                 Fock_matrix_mo_pos,size(Fock_matrix_mo_pos,1))
END_PROVIDER

