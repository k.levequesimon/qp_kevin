program fock_pos
  implicit none
  BEGIN_DOC
! TODO : Put the documentation of the program here
  END_DOC
  integer :: i,j
!  do i = 1, ao_num
!   do j = 1, ao_num
!    print*,i,j,ao_pos_integrals(i,j)
!   enddo
!  enddo
  call hcore_guess
  call hcore_guess_pos
!  provide Fock_matrix_ao_pos Fock_matrix_ao_w_pos_alpha Fock_matrix_ao_w_pos_beta 
  print*,''
  print*,'Fock matrix for positrons '
  print*,''
  do i = 1, ao_num
   do j = 1, ao_num
    print*,i,j,Fock_matrix_ao_pos(j,i)
   enddo
  enddo

  print*,''
  print*,'Fock matrix for electrons '
  print*,''
  do i = 1, ao_num
   do j = 1, ao_num
    print*,i,j,Fock_matrix_ao_w_pos_alpha(j,i)
   enddo
  enddo
!
!  print*,''
!  print*,'Fock matrix for electrons without positrons'
!  print*,''
!  do i = 1, ao_num
!   do j = 1, ao_num
!    print*,i,j,Fock_matrix_ao_alpha(j,i)
!   enddo
!  enddo
!  provide eigenvectors_Fock_matrix_mo_pos
!  provide eigenvectors_fock_matrix_mo_e_w_pos
!  print*,'SCF_energy_pos = ',SCF_energy_pos
!  do i = 1, mo_num
!   do j = 1, mo_num
!    print*,'i,j,',i,j,Fock_matrix_mo_pos(j,i)
!   enddo
!  enddo
end
