program damp_scf_pos
  BEGIN_DOC
! Level-shift damped electron-positron SCF algorithm 
  END_DOC
  call create_guess_elec
!  call create_guess_pos
  call orthonormalize_mo_pos
  call orthonormalize_mos
  call run
end

subroutine run
  BEGIN_DOC
!   Run SCF calculation
  END_DOC
  implicit none
  mo_label = 'Canonical'
  call damping_SCF_pos
  call orthonormalize_mo_pos
end


